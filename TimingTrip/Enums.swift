//
//  Enums.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 28/08/15.
//  Copyright (c) 2015 Frobenious. All rights reserved.
//

import Foundation
import UIKit


enum MiniGameType: UInt32 {
    
    case SailTheSea
    case Feudalism
    case Greece
    
    // This counts the total of Minigames enumerated
    static let count: UInt32 = {
        var max: UInt32 = 0
        while let _ = MiniGameType(rawValue: max) { max += 1 }
        return max
    }()
    
    /// This returns a random Minigame
    static func random() -> MiniGameType {
        let max = MiniGameType.count
        let gameType = arc4random_uniform(max)
        return MiniGameType(rawValue: gameType)!
    }
    static func onSequence() -> MiniGameType {
        struct gameNumber {
            static var number: UInt32 = 0
        }
        let number = gameNumber.number
        gameNumber.number++
        if (gameNumber.number == MiniGameType.count) {
            gameNumber.number = 0
        }
        return MiniGameType(rawValue: number)!
    }
}

enum Scenes: UInt32{
    case TransitionScene
    case GameOver
    case None
}

enum MinigameStatus{
    case Success
    case Failure
    case GameOver
    case NewGame
}

enum Difficulty{
    case Tutorial
    case Easy
    case Medium
    case Hard
    case VeryHard
}
//
//  Constants.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 12/22/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import Foundation

/** This is for constants that are being used in multiple files. Put them here for easier access 
*/
struct Constants{
    
    // MARK: Example 1
    // static let var1 : CGFloat = 0
    // static let var2 : String = "teste"
    
    // MARK: Example 2
    // static let var3 = 0
}
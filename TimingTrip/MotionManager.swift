//
//  MotionManager.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 9/1/15.
//  Copyright (c) 2015 Frobenious. All rights reserved.
//

import UIKit
import CoreMotion

/** This manages the device's accelerometer, magnetometer and gyroscope
*/
class MotionManager: NSObject {
    static let sharedInstance = MotionManager()
    var manager = CMMotionManager()
}


class MedalsViewed: NSObject {
    static let sharedInstance = MedalsViewed()
    var medals = [0,0,0]

}

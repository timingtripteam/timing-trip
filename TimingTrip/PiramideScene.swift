//
//  PiramideScene.swift
//  TimingTrip
//
//  Created by Ricardo Z Charf on 8/20/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//


import Foundation
import SpriteKit

/* DEPRECATED
class PiramideScene:MGScene{
    
    var pyramidParts:[SKSpriteNode]! = []
    
    var ground:SKSpriteNode!
    var rope:SKNode!
    
    var ropeGraphic:SKSpriteNode!
    var sandEffect:SKSpriteNode!
    var sun:SKSpriteNode!
    var currentPiece = 1
    
    var wrongMarker:SKNode!
    
    var leftBound:SKNode!
    var rightBound:SKNode!
    
    var isAnimating = false
    
   
    
    override func didMoveToView(view: SKView) {
        print(UIScreen.mainScreen().bounds.size);
        //FIXME: Pyramid not using the miniGame Interface
        //self.timer = GameTimer(delegate: self)
        minigameStatus = .Failure
        
        self.ground = self.childNodeWithName("ground") as? SKSpriteNode
        self.rope = self.childNodeWithName("rope")
        self.pyramidParts.append(ground)
        self.pyramidParts.append(self.childNodeWithName("part1") as! SKSpriteNode)
        self.pyramidParts.append(self.childNodeWithName("part2") as! SKSpriteNode)
        self.pyramidParts.append(self.childNodeWithName("part3") as! SKSpriteNode)
        self.leftBound = self.childNodeWithName("leftBound")
        self.rightBound = self.childNodeWithName("rightBound")
        self.wrongMarker = self.childNodeWithName("wrong")
        self.sandEffect = self.childNodeWithName("sandEffect") as! SKSpriteNode
        pyramidParts[1].position = rope.position
        
        
        let dif:Double = (2 * ( getIntFromDifficulty() ) )
        
        let time:Double = 6 - dif
        
        
        //timerGauge.runAction(move)
        calculateRopeMovement()
        //self.timer.fireTimerWith(Float(time+1.7*2))
        
        self.sun = self.childNodeWithName("sun") as! SKSpriteNode
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
    }
    
   /* override func gameTimerHasUpdated(timeInPercentage: Float, secondsLeft: Float) {
        //super.gameTimerDidUpdate(timeInPercentage, secondsLeft: secondsLeft)
        
        //nothing ????
    }*/
    
    func calculateRopeMovement(){
        let dif:Double = (0.75 * ( getIntFromDifficulty() ) )
        let time:Double  = 1 - dif
        _ = rope.position.x - leftBound.position.x
        let fraction:Double = Double((rope.position.x - leftBound.position.x)/(rightBound.position.x - leftBound.position.x))
        let moveToSide = SKAction.moveTo(CGPoint(x: CGFloat(leftBound.position.x + CGFloat(pyramidParts[currentPiece].size.width/2)), y: rope.position.y), duration: time*fraction)
        let moveRight = SKAction.moveToX(CGFloat(rightBound.position.x - CGFloat(pyramidParts[currentPiece].size.width/2)), duration: time)
        let moveLeft = SKAction.moveToX(CGFloat(leftBound.position.x + CGFloat(pyramidParts[currentPiece].size.width/2)), duration: time)
        let final:SKAction = SKAction.sequence([moveToSide,SKAction.repeatActionForever(SKAction.sequence([moveRight,moveLeft]))])
        pyramidParts[currentPiece].runAction(final)
        rope.runAction(final)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            _ = self.nodeAtPoint(location)
            if(!isAnimating)
            {
                isAnimating = true
                fallAnimation(){
                    self.animateNewPiece({ () -> () in
                        self.calculateRopeMovement()
                        self.isAnimating = false
                    })
                    
                }
            }
        }
    }
    
    func fallAnimation(callback:()->()){
        pyramidParts[currentPiece].removeAllActions()
        rope.removeAllActions()
        let newPos:Double = Double(pyramidParts[currentPiece-1].position.y + pyramidParts[currentPiece-1].size.height/2 + pyramidParts[currentPiece].size.height/2)
        let action = SKAction.moveTo(CGPoint(x: Double(pyramidParts[currentPiece].position.x), y: newPos), duration: 0.4)
        let block = SKAction.runBlock { () -> Void in
            callback()
        }
        let sound = SKAction.playSoundFileNamed("fallingSound.wav", waitForCompletion: false)
        let sound2 = SKAction.playSoundFileNamed("hitSound.wav", waitForCompletion: false)
        let group = SKAction.group([action,block,sound])
        let seq = SKAction.sequence([group,sound2])
        
        pyramidParts[currentPiece].runAction(seq, completion: { () -> Void in
            if(self.checkForPyramidFloorCorrectude()){
                self.animateSandEffect(self.currentPiece)
            }
        })
    }
    
    func changePiece(callback:()->()){
        currentPiece++
        if(currentPiece >= pyramidParts.count){
            finishMiniGameWithSuccess()
            return
        }
        
        let horizontalSpace:CGFloat = (rightBound.position.x - leftBound.position.x) - pyramidParts[currentPiece].size.width
        let randomX = CGFloat(arc4random_uniform(UInt32(horizontalSpace)))
        rope.position.x = leftBound.position.x + randomX
        pyramidParts[currentPiece].position = rope.position
        callback()
    }
    
    func finishMiniGameWithSuccess(){
        self.minigameStatus = .Success
        
        isAnimating=true
        //self.timer.endTimer()
        self.timerGauge.removeAllActions()
        self.runAction(SKAction.waitForDuration(0.5), completion: { () -> Void in
            self.minigameHasEnded()
        })
    }
    
    
    func checkForPyramidFloorCorrectude()->Bool{
        if(currentPiece>1){
            let percentage = CGFloat(0.35)
            let distanceLimit:CGFloat = percentage * (pyramidParts[currentPiece-1].size.width/2)
            let realDistance:CGFloat = abs(pyramidParts[currentPiece].position.x - pyramidParts[currentPiece-1].position.x)
            if(realDistance <= distanceLimit){
                return true
            }else{
                self.timerGauge.removeAllActions()
                return false;
            }
        }else{
            return true
        }
    }
    
    func animateNewPiece(callback:()->()){
        let time:Double = 0.5
        let targetY = screenHeight + pyramidParts[currentPiece].size.height
        let vertical = rope.position.y
        let upAction = SKAction.moveToY(targetY, duration: time)
        let downAction = SKAction.moveToY(vertical, duration: time)
        if(!self.checkForPyramidFloorCorrectude()){
            self.failAimation({ () -> () in
                self.minigameStatus = .Failure
                self.miniGameHasEnded()
            })
        }else{
            rope.runAction(upAction, completion: { () -> Void in
                self.changePiece({ () -> () in
                    self.pyramidParts[self.currentPiece].runAction(downAction)
                    self.rope.runAction(downAction, completion: { () -> Void in
                        callback()
                    })
                })
            })
        }
    }
    
    func failAimation(callback:()->()){
        self.wrongMarker.position = CGPoint(x: 768, y: 1024)
        self.wrongMarker.runAction(SKAction.waitForDuration(2.0), completion: { () -> Void in
            callback()
        })
    }
    
    func getIntFromDifficulty() -> Double{
        switch(self.difficulty!)
        {
        case Difficulty.Easy :
            return 0
        case Difficulty.Medium :
            return 0.33
        case Difficulty.Hard :
            return 0.66
        case Difficulty.VeryHard :
            return 1.0
        default:
            return -1.0
        }
    }
    
    func animateSandEffect(part:Int){
        let orgX = self.sandEffect.xScale
        let orgY = self.sandEffect.yScale
        let orgPos = self.sandEffect.position
        let time:Double = 0.6
        let growx = SKAction.scaleXTo(3.7, duration: time)
        let growy = SKAction.scaleYTo(4.5, duration: time)
        let alpha = SKAction.fadeOutWithDuration(time)
        let group = SKAction.group([growx,growy,alpha])
        let pos = CGFloat(pyramidParts[part].position.y - pyramidParts[part].size.height/2)
        self.sandEffect.position = CGPoint(x: pyramidParts[part].position.x, y: pos)
        self.sandEffect.runAction(group, completion: { () -> Void in
            self.sandEffect.xScale = orgX
            self.sandEffect.yScale = orgY
            self.sandEffect.position = orgPos
            self.sandEffect.alpha = 1
        })
    }
}*/
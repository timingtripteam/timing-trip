//
//  Feudalism.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 27/12/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import SpriteKit
import UIKit

enum FeudalismPhysics:UInt32 {
    case None = 0
    case Wheat = 1
    case Basket = 2
}

class Feudalism: MGScene {
    
    var basket: SKSpriteNode!
    var wheatCounter = 0
    var touchedWheatPosition = CGPointZero
    var touchedNode: SKNode?
    var cloudArray = [SKSpriteNode]()
    
    override func didMoveToView(view: SKView) {
        self.actionText = "Colete o trigo"
        self.instructionText = "Arraste o trigo para o cesto\n antes do senhor feudal chegar."
        super.didMoveToView(view)
        

        minigameStatus = .Failure
        
        self.actionLabel.fontColor = UIColor.blackColor()
        self.actionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: 1.65*CGRectGetMidY(self.visibleArea))
        
        physicsWorld.contactDelegate = self
        
        configurePhysics()
        configureClouds()
        
        
        
        let time = getTimeFromDifficulty(difficulty)
        configureSr(time)
        timerBar.startTimerWith(time)
        
    }
    
    override func createInstructionLabel() {
        self.instructionLabel.text = instructionText
        self.instructionLabel.fontSize = 0.03*self.visibleArea.height
        self.instructionLabel.fontColor = UIColor.blackColor()
        self.instructionLabel.name = "tutorialLabel"
        self.instructionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: 1.55*CGRectGetMidY(self.visibleArea))
        self.instructionLabel.zPosition = 70
        self.instructionLabel.setShadow()
        self.instructionLabel = self.instructionLabel.multipleLineText()
        self.instructionLabel.runAction(SKAction.fadeAfterDuration(0, duration: getTimeFromDifficulty(self.difficulty)/2))
        self.worldNode.addChild(self.instructionLabel)
    }
    
    func configureSr(time: NSTimeInterval) {
        guard let spotsNode = childNodeWithName("Spots") else {
            fatalError("No spots found")
        }
        guard let srNode = worldNode.childNodeWithName("//Sr") else {
            fatalError("No Sr found")
        }
        
        
        let srTextures = loadTexturesFromAtlasWithName("feudalism-sr")
        
        let interval = (time-1.5)/Double(spotsNode.children.count)
        
        var spots = [SKNode]()
        for i in 1...spotsNode.children.count {
            let spot = spotsNode.childNodeWithName("spot\(i)")!
            spots.append(spot)
        }
        
    
        let AddZAction = SKAction.runBlock({
            srNode.zPosition += 4
            
        })
        
        var moveOnFirstHill = [SKAction]()
        
        //First Hill
        for i in 0...3 {
            let moveTo = SKAction.moveTo(spots[i].position, duration: interval)
            moveOnFirstHill.append(moveTo)
        }
        let scaleOnFirstHill = SKAction.scaleXTo(-0.4, y: 0.4, duration: interval*4)
        //Go to front of second hill and changes scale
        var moveOnSecondHill = [SKAction]()

        //Second Hill
        for i in 4...6 {
            let moveTo = SKAction.moveTo(spots[i].position, duration: interval)
            moveOnSecondHill.append(moveTo)
        }
        let scaleOnSecondHill = SKAction.scaleXTo(0.6, y: 0.6, duration: interval*3)

        
        //Third Hill
        //Final move
        let moveOnThirdHill = SKAction.moveTo(spots[7].position, duration: interval)
        let scaleOnThirdHill = SKAction.scaleXTo(0.8, y: 0.8, duration: interval)

        
        //Movement
        srNode.runAction(SKAction.group([SKAction.sequence(moveOnFirstHill),scaleOnFirstHill]), completion: {
            [unowned self] in
            srNode.runAction(AddZAction)
            srNode.xScale *= -1
            srNode.runAction(SKAction.group([SKAction.sequence(moveOnSecondHill),scaleOnSecondHill]), completion: {
                [unowned self] in
                srNode.runAction(AddZAction)
                srNode.runAction(SKAction.group([moveOnThirdHill,scaleOnThirdHill]), completion:  {
                    [unowned self] in
                    srNode.removeActionForKey("animation")
                    if (self.minigameStatus == .Success) {
                        (srNode as! SKSpriteNode).texture = SKTexture(imageNamed: "sr_happy")
                        srNode.runAction(SKAction.waitForDuration(0.4), completion: {
                            [unowned self] in
                            self.minigameHasEnded()
                        })
                    } else {
                        (srNode as! SKSpriteNode).texture = SKTexture(imageNamed: "sr_idle")
                        srNode.runAction(SKAction.colorizeWithColor(UIColor.redColor(), colorBlendFactor: 0.6, duration: 0.4), completion: {
                            [unowned self] in
                            self.minigameHasEnded()
                        })
                    }
                })
            })
        })
  
        //Animation
        srNode.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(srTextures, timePerFrame: 0.04)), withKey: "animation")
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        guard let touchLocation = touches.first?.locationInNode(worldNode) else {
            return
        }
        
        let nodes = worldNode.nodesAtPoint(touchLocation)
        for node in nodes {
            if node.name == "Wheat" {
                touchedNode = node
                touchedWheatPosition = node.position
            }
        }
        
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        guard let previousLocation = touches.first?.previousLocationInNode(worldNode) else {
            return
        }
        guard let touchLocation = touches.first?.locationInNode(worldNode) else {
            return
        }
        
        if touchedNode != nil {
            touchedNode!.position.x += touchLocation.x - previousLocation.x
            touchedNode!.position.y += touchLocation.y - previousLocation.y
            
        }
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        if touchedNode != nil {
            let wheat = touchedNode!
            wheat.runAction(SKAction.fadeOutWithDuration(0.2), completion: {
                [unowned self] in
                wheat.position = self.touchedWheatPosition
                wheat.runAction(SKAction.fadeInWithDuration(0.2))
            })
        }
        touchedNode = nil
        
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        if touchedNode != nil {
            let wheat = touchedNode!
            wheat.runAction(SKAction.fadeOutWithDuration(0.2), completion: {
                [unowned self] in
                wheat.position = self.touchedWheatPosition
                wheat.runAction(SKAction.fadeInWithDuration(0.2))
                })
        }

        touchedNode = nil
        
    }
    
    
    func configureClouds() {
        guard let cloud1 = self.worldNode.childNodeWithName("cloud1"), cloud2 = self.worldNode.childNodeWithName("cloud2") else {
            fatalError("No Clouds found")
        }
        
        let clouds = [cloud1,cloud2]
        
        var time: NSTimeInterval = 10
        var cloudSpeed: Double = 0

        
        for cloud in clouds {
            cloudSpeed = Double(visibleArea.width+cloud.frame.width)/time
            let position = arc4random_uniform(UInt32(visibleArea.width))
            cloud.position.x = CGFloat(position) + visibleArea.origin.x
            
            let initialTime = Double((cloud.position.x - visibleArea.origin.x))/cloudSpeed
            let positionFinal = visibleArea.origin.x
            let moveAction = SKAction.moveToX(positionFinal, duration: initialTime)
            let resetPosition = visibleArea.origin.x+visibleArea.width+cloud.frame.width
            let resetAction = SKAction.runBlock({
                cloud.position.x = resetPosition
            })
            
            
            
            let initialSequence = SKAction.sequence([moveAction,resetAction])
            
            let moveFinal = SKAction.moveToX(positionFinal, duration: time)

            let repeatSequence = SKAction.sequence([moveFinal,resetAction])
            cloud.runAction(initialSequence, completion: {
                cloud.runAction(SKAction.repeatActionForever(SKAction.repeatActionForever(repeatSequence)))
            })
            time *= 1.7
        }
        
    }
    func configurePhysics() {
        guard let wheatNode = self.worldNode.childNodeWithName("WheatNode") else {
            fatalError("No Wheat Node found")
        }
        guard let basketNode = self.worldNode.childNodeWithName("//WheatBag") as? SKSpriteNode else {
            fatalError("No Wheat Bag found")
        }
        basket = basketNode
        
        for node in wheatNode.children {
            node.physicsBody = getWheatPhysicsBody(node as! SKSpriteNode)
        }
        
        let basketBody = SKPhysicsBody(rectangleOfSize: basket.size)
        basketBody.allowsRotation = false
        basketBody.affectedByGravity = false
        basketBody.categoryBitMask = FeudalismPhysics.Basket.rawValue
        basketBody.collisionBitMask = FeudalismPhysics.None.rawValue
        basketBody.contactTestBitMask = FeudalismPhysics.None.rawValue
        basket.physicsBody = basketBody
        
        
    }
    
    //do nothing
    override func timerHasFinished() {
        
    }

    func getWheatPhysicsBody(node: SKSpriteNode) ->SKPhysicsBody {
        let body = SKPhysicsBody(rectangleOfSize: node.size)
        body.allowsRotation = false
        body.affectedByGravity = false
        body.categoryBitMask = FeudalismPhysics.Wheat.rawValue
        body.collisionBitMask = FeudalismPhysics.None.rawValue
        body.contactTestBitMask = FeudalismPhysics.Basket.rawValue
        return body
    }
    
    func handleWheatAndBasketCollision(bodyA: SKPhysicsBody, bodyB: SKPhysicsBody) {
        guard let nodeA = bodyA.node, let nodeB = bodyB.node else {
            return
        }
        if nodeA == basket {
            nodeB.removeFromParent()
        }
        else {
            nodeA.removeFromParent()
        }
        
        let scaleSequence = SKAction.sequence([SKAction.scaleTo(1.1, duration: 0.3), SKAction.scaleTo(1, duration: 0.3)])
        basket.runAction(scaleSequence, completion:  {
            [unowned self] in
            self.wheatCounter++
            if self.wheatCounter == 6 {
                self.minigameStatus = .Success
            }
            })
    }
    
}

extension Feudalism: SKPhysicsContactDelegate {
    func didBeginContact(contact: SKPhysicsContact) {
        let collision = contact.bodyA.categoryBitMask|contact.bodyB.categoryBitMask
        
        let wheatAndBasket = FeudalismPhysics.Wheat.rawValue|FeudalismPhysics.Basket.rawValue
        
        
        if collision == wheatAndBasket {
            handleWheatAndBasketCollision(contact.bodyA, bodyB: contact.bodyB)
        }
    }
    
    
    
}


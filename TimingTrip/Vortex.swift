//
//  Vortex.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 1/21/16.
//  Copyright © 2016 TimingTripTeam. All rights reserved.
//

import UIKit
import SpriteKit

class Vortex: SKNode {
    var middleNode: SKNode!
    var posInitial: CGPoint!
    var positions: SKNode!
    var rings = [SKNode]()
    
    override init() {
        super.init()
        
        let reference = SKReferenceNode(fileNamed: "Vortex")
        middleNode = reference.childNodeWithName("//middleNode")!
        positions = reference.childNodeWithName("//Positions")!
        posInitial = positions.childNodeWithName("posInitial")!.position
        positions.childNodeWithName("posInitial")!.removeFromParent()
        positions.removeFromParent()
        middleNode.removeFromParent()
        middleNode.childNodeWithName("vortexHole")!.removeFromParent()
        addChild(middleNode)
        startRingsAnimation()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setSize(size: CGSize) {
        let node = middleNode.childNodeWithName("ring0") as! SKSpriteNode
        let scale = node.size.height / size.height
        
        
        middleNode.xScale = scale
        middleNode.yScale = scale

    }
    
    func followWay(node: SKSpriteNode, time: NSTimeInterval) -> SKAction {
        var points = [CGPoint]()
        
        for i in (0...positions.children.count-1).reverse() {
            
            var point = convertPoint(positions.childNodeWithName("pos\(i)")!.position, toNode: self.parent!)
            point.x -= 20
            points.append(point)
        }
        
        let interval = Double(time/Double(points.count-1))
        
        var actions = [SKAction]()
        
        let factor = (1 - node.xScale)/CGFloat(points.count-1)
        var scale = node.xScale+factor
        for point in points {
            let actionMove = SKAction.moveTo(point, duration: interval)
            
            if (scale > 0.9 || scale > 1) {
                scale = 1
            }
            let actionScale = SKAction.scaleTo(scale, duration: interval)
            scale += factor
            let group = SKAction.group([actionMove,actionScale])
            actions.append(group)
        }
        
        let action = SKAction.sequence(actions)
        
        node.position = posInitial
        node.alpha = 0
        return action
    }
    
    
    // Mark: Rings Animation
    func startRingsAnimation() {
        for i in (0...5).reverse() {
            
            let node = middleNode.childNodeWithName("ring\(i)")!
            if(i != 5) {
                node.alpha = 0
            }
            rings.append(node)
        }
        let actions = getPosAndScaleActions()
        
        
        //reversed. Ring 0 = smallest ring
        let initialNode = self.rings[0]
        let initialPosition = initialNode.position
        let initialScale = initialNode.xScale
        let resetBlock = SKAction.customActionWithDuration(0.00001, actionBlock: {(actionNode,time) -> Void in
            actionNode.removeActionForKey("fade")
            actionNode.zPosition = 10
            actionNode.position = initialPosition
            actionNode.xScale = initialScale
            actionNode.yScale = initialScale
            actionNode.alpha = 1
            
        })
        
        for i in 1...rings.count-1 {
            rings[i].zPosition = 10
            rings[i].position = initialPosition
            rings[i].xScale = initialScale
            rings[i].yScale = initialScale
        }
        runAnimationOnRings(actions, resetBlock: resetBlock)
    }
    func runAnimationOnRings(animation: [SKAction], resetBlock: SKAction) {
        //Fade, scale and reset position
        //let fade = SKAction.fadeOutWithDuration(totalTime)
        //let block = SKAction.group([SKAction.sequence(animation),fade])
        let sequenceWithReset = SKAction.sequence([SKAction.sequence(animation),resetBlock])
        let completeAnimation = SKAction.repeatActionForever(sequenceWithReset)
        
        rings[0].runAction(completeAnimation)
        
        let waitTime = totalTime/Double(numberOfRings)
        
        for i in 1...rings.count-1 {
            
            rings[i].runAction(SKAction.waitForDuration(Double(i)*waitTime), completion:{
                [unowned self] in
                self.rings[i].alpha = 1
                self.rings[i].runAction(completeAnimation)
                })
        }
        
        
    }
    
    let totalTime = 1.5
    let numberOfRings: Double = 6
    
    func getPosAndScaleActions() -> [SKAction] {
        let time: NSTimeInterval = totalTime/Double(numberOfRings)
        
        var actions = [SKAction]()
        var i = 0
        for node in rings {
            let scaleAction = SKAction.scaleTo(node.xScale, duration: time)
            let moveAction = SKAction.moveTo(node.position, duration: time)
            if i == 3 {
                //Changes zPosition
                let block = SKAction.customActionWithDuration(0.00001, actionBlock: {(actionNode,time) -> Void in
                    actionNode.zPosition = 21
                    //let fadeTime = (ringsNumber-Double(i))*Double(time)
                    actionNode.runAction(SKAction.fadeOutWithDuration(1), withKey: "fade")
                })
                let group = SKAction.group([scaleAction, moveAction, block])
                actions.append(group)
            } else {
                let group = SKAction.group([scaleAction,moveAction])
                actions.append(group)
            }
            i++
            
            
        }
        
        
        
        return actions
    }
}

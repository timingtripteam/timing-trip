//
//  StartScene.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 8/19/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import UIKit

class StartScene: SKScene, ButtonDelegate {
    var minigameScebeDelegate:MGSceneDelegateProtocol? = nil
    var debugDelegate: DebugDelegate?
    var debugModeLabel = SKLabelNode()
    
    var newMedals: Int? {
        didSet {
            updateNotification()
        }
    }
    var settings: Button?
    var timeLine: Button?
    var gameCenter: Button?
    
    let BUTTON_WIDTH: CGFloat = 0.084*UIScreen.mainScreen().bounds.height
    var BUTTON_SIZE: CGSize {
        return CGSizeMake(BUTTON_WIDTH, BUTTON_WIDTH)
    }
    
    override func didMoveToView(view: SKView) {
        self.userInteractionEnabled = false        
        createContent()
        presentButtons()
    }
    
    /** Function that is called when the user has touched the screen.
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in (touches) {
            let touchLocation = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(touchLocation)
           
            if(touchedNode.name != nil){
                //print("\tI've touched: \(touchedNode.name!)")
                
                switch(touchedNode.name!){
                case "debugModeLabel":
                    if(debugModeLabel.text == "Debug Mode: OFF"){
                        debugModeLabel.text = "Debug Mode: ON"
                        debugDelegate?.changeDebug()
                    }else{
                        debugModeLabel.text = "Debug Mode: OFF"
                        debugDelegate?.changeDebug()

                    }
                    break
                case "HUDcontainer":
                    break
                default:
                    self.minigameScebeDelegate?.minigameFinishedWithStatus(.NewGame)
                    break
                }
            } else {
                self.minigameScebeDelegate?.minigameFinishedWithStatus(.NewGame)
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func createContent(){
        //self.backgroundColor = UIColor.wetAsphaltColor()
        setBackgroundImage()
        createTitle()
        createTapToStartLabel()
        //createDebugLabel()
        createButtons()
    }
    
    func setBackgroundImage(){
        let bgImage = SKSpriteNode(imageNamed: "startsceneBG")
        bgImage.position = CGPoint(x:CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        bgImage.name = "backgroundImage"
        bgImage.zPosition = -100
        let scale = bgImage.size.height/self.view!.frame.height
        bgImage.xScale /= scale
        bgImage.yScale /= scale
        self.addChild(bgImage)
    }
    
    func createTitle(){
        let titleLabel = SKLabelNode(fontNamed:"Helvetica Neue Condensed Black")
        titleLabel.text = "Timing Trip"
        titleLabel.fontSize = 0.09 * self.frame.size.height
        titleLabel.name = "gameTitle"
        titleLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.70*CGRectGetMidY(self.frame))
        titleLabel.zPosition = 10
        titleLabel.setShadow()
        self.addChild(titleLabel)
        
        let goDown = SKAction.moveBy(CGVectorMake(0, -titleLabel.frame.height/10), duration: 0.8)
        let goUp = SKAction.moveBy(CGVectorMake(0, titleLabel.frame.height/10), duration: 0.6)
        let bounce = SKAction.sequence([goDown, goUp])
        
        titleLabel.runAction(SKAction.repeatActionForever(bounce))
    }
    
    func createDebugLabel(){
        debugModeLabel = SKLabelNode(fontNamed:"Familiar Pro")
        debugModeLabel.text = "Debug Mode: OFF";
        debugModeLabel.fontSize = 15
        debugModeLabel.name = "debugModeLabel"
        debugModeLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.225*CGRectGetMidY(self.frame))
        self.addChild(debugModeLabel)
    }
    
    func createTapToStartLabel(){
        let tapToStartLabel = SKLabelNode(fontNamed:"Familiar Pro")
        tapToStartLabel.text = "TOQUE PARA INICIAR";
        tapToStartLabel.fontSize = 0.03 * self.frame.size.height
        tapToStartLabel.name = "tapToStartLabel"
        tapToStartLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.50*CGRectGetMidY(self.frame))
        tapToStartLabel.setShadow()
        self.addChild(tapToStartLabel)
        
        let disappear = SKAction.fadeAlphaTo(0.0, duration: 0.6)
        let appear = SKAction.fadeAlphaTo(1.0, duration: 0.3)
        let pulse = SKAction.sequence([disappear, appear])
        
        tapToStartLabel.runAction(SKAction.repeatActionForever(pulse))
    }
    
    func createButtons(){
        settings = Button(imageNamed: "settings", size: BUTTON_SIZE)
        settings!.name = "settings"
        settings!.delegate = self
        
        timeLine = Button(imageNamed: "threeDots", size: BUTTON_SIZE)
        timeLine!.name = "timeLine"
        timeLine!.delegate = self
        
        gameCenter = Button(imageNamed: "gameCenter", size: BUTTON_SIZE)
        gameCenter!.name = "gameCenter"
        gameCenter!.delegate = self
    }
    
    func presentButtons() {
        let timeToShow = 0.3
        
        let spaceBetween = (self.frame.width - 3*BUTTON_WIDTH)/4.0
        
        settings!.position = CGPointMake(spaceBetween + BUTTON_WIDTH/2, 0)
        timeLine!.position = CGPointMake(settings!.position.x + BUTTON_WIDTH + spaceBetween, 0)
        gameCenter!.position = CGPointMake((timeLine?.position.x)! + BUTTON_WIDTH + spaceBetween, 0)
        
        let fadeIn = SKAction.fadeInWithDuration(2*timeToShow)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: 1.4*BUTTON_WIDTH), duration: timeToShow)
        let showUp = SKAction.group([fadeIn,moveUp])
    
        let nodesToAnimate = [settings, timeLine, gameCenter]

        let containerNode = SKNode()
        containerNode.name = "HUDcontainer"
        for var i=0; i<nodesToAnimate.count; i++ {
            containerNode.addChild(nodesToAnimate[i]!)
        }
        self.addChild(containerNode)
        containerNode.alpha = 0
        containerNode.runAction(showUp) { () -> Void in
            self.userInteractionEnabled = true
        }
        
        //new medals notification
        if let number = newMedals {
            if number > 0 {
                let notification = SKSpriteNode(imageNamed: "medalNotification")
                notification.name = "notification"
                let notificationSize = CGSizeMake(BUTTON_SIZE.width/3, BUTTON_SIZE.height/3)
                notification.size = notificationSize
                notification.position.x = timeLine!.frame.width/2 - notification.size.width/2
                notification.position.y = timeLine!.frame.height/2 - notification.size.height/2
                notification.zPosition = 1
                timeLine!.addChild(notification)
                
                let string = "\(number)"
                let label = SKLabelNode(text: string)
                label.name = "label"
                label.zPosition = 1
                label.fontName = "Familiar PRO-Bold"
                label.position.y -= label.frame.height/4
                label.fontColor = UIColor.whiteColor()
                label.fontSize = 16
                notification.addChild(label)
            }
        }
        
    }
    func updateNotification() {
        if timeLine != nil {
            if newMedals > 0 {
                if (timeLine!.childNodeWithName("notification")!.hidden) {
                    timeLine!.childNodeWithName("notification")!.hidden = false
                }
                (timeLine!.childNodeWithName("notification")!.childNodeWithName("label")! as! SKLabelNode).text = "\(newMedals!)"
            } else {
                timeLine!.childNodeWithName("notification")?.hidden = true
            }
        }
    }

    func performSegueToGallery() {
        let currentViewController:UIViewController = UIApplication.sharedApplication().keyWindow!.rootViewController!
        currentViewController.performSegueWithIdentifier("gallerySegue", sender: nil)
    }
    /** This treats what to do when buttons are tapped
    */
    func buttonTapped(button: Button){
        if(button.name != nil){
            switch(button.name!){
            case "settings":
                print("Dei tap no settings")
                self.showAlertWithMessage("Logo teremos configurações!")
            case "timeLine":
                print("Dei tap no timeLine")
                self.performSegueToGallery()
            case "gameCenter":
                print("Dei tap no gameCenter")
                self.showAlertWithMessage("Logo teremos Game Center!")
            default:
                print("default")
            }
        }
    }
    /** Create and show a AlertView
     */
    func showAlertWithMessage(message: String) {

        let alert = UIAlertController(title: "Ops! =P", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        // show the alert
        self.view?.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
}

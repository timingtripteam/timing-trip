//
//  Grrece.swift
//  TimingTrip
//
//  Created by Túlio Bazan da Silva on 07/01/16.
//  Copyright © 2016 TimingTripTeam. All rights reserved.
//

import SpriteKit
import UIKit

class Greece: MGScene {
    
    var peopleSpawner: SKNode!
    var t = NSTimeInterval()
    var nBlues = 0
    var nReds = 0
    var nPerson = 5
    var touchedNode: SKNode?
    
    var redBT: Button?
    var blueBT: Button?
    var buttonView: SKSpriteNode?
    
    var BUTTON_WIDTH: CGFloat {
        return 0.15*self.visibleArea.width
    }
    var BUTTON_SIZE: CGSize {
        return CGSizeMake(BUTTON_WIDTH, BUTTON_WIDTH)
    }
    
    //Textures
    var greekBlue: [SKTexture]!
    var greekRed: [SKTexture]!
    
    
    
    override func didMoveToView(view: SKView) {
        self.actionText = "Comande a assembléia"
        self.instructionText = "Identifique qual cor representa\n a maioria dos cidadãos."
        super.didMoveToView(view)
        

        // time each rock takes to cross the screen
        let time = self.getTimeFromDifficulty(self.difficulty)/4
        
        let fade = SKAction.fadeAfterDuration(0, duration:time*0.5+1.5)
        let moveUp = SKAction.moveToX(1.2*CGRectGetMidX(self.visibleArea), duration: 1.1)
        let sequence = SKAction.sequence([moveUp,fade])
        
        self.actionLabel.removeAllChildren()
        self.actionLabel.fontSize *= 0.67
        self.actionLabel.setShadow()
        self.actionLabel.position = CGPoint(x:2*CGRectGetMidX(self.visibleArea), y: 0.4*CGRectGetMidY(self.visibleArea))
        self.actionLabel.removeAllActions()
        self.actionLabel.runAction(sequence)
        
//        moveUp = SKAction.moveToX(1.2*CGRectGetMidX(self.visibleArea), duration: 1)
//        sequence = SKAction.sequence([moveUp,wait,fade])
//        
        self.instructionLabel.removeFromParent()
        self.instructionLabel.text = self.instructionText
        self.instructionLabel.removeAllChildren()
        self.instructionLabel.removeAllActions()
        self.instructionLabel.fontSize *= 0.75
        self.instructionLabel.position = CGPoint(x:2*CGRectGetMidX(self.visibleArea), y: 0.33*CGRectGetMidY(self.visibleArea))
        self.instructionLabel.setShadow()
        self.instructionLabel = self.instructionLabel.multipleLineText()
        self.instructionLabel.removeAllActions()
        self.instructionLabel.runAction(sequence)
        
        self.worldNode.addChild(self.instructionLabel)
        
        physicsWorld.contactDelegate = self
        greekBlue = loadTexturesFromAtlasWithName("greekBlue")
        greekRed = loadTexturesFromAtlasWithName("greekRed")
        
        
        configureScene()
        spawnPeople()

        timerBar.startTimerWith(getTimeFromDifficulty(difficulty))
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
       touchedNode = nil
        
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        
        touchedNode = nil
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        touchedNode = nil
        
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        touchedNode = nil
        
    }
    
    
    func configureScene() {
        guard let spawnerNode = self.worldNode.childNodeWithName("PeopleSpawner") else {
            fatalError("No Wheat Node found")
        }
        
        peopleSpawner = spawnerNode
        print("\(spawnerNode.position)")
        
    }
    
    func spawnPeople(){
        let personPeriod = Double(getTimeFromDifficulty(difficulty))
        self.peopleSpawner.runAction(SKAction.waitForDuration(4), completion: {
            [unowned self] in
            self.spawnPerson(1/2*personPeriod/Double(self.nPerson))
        })
    }

    //Create a person
    func spawnPerson(time: Double) {
        
        //Create a person
        let person = colorSelected(arc4random_uniform(2))
        person.xScale *= -1
        
        person.anchorPoint = CGPointMake(0.5, 0.5)
        person.zPosition = 5
        let posY = CGFloat.random(peopleSpawner.position.y * 0.8, to: peopleSpawner.position.y * 1.2)
        let posX = peopleSpawner.position.x
        person.position = CGPointMake(posX, posY)
        
        //Move the person from the Rigth to the Left
        let movePersonLeft = SKAction.moveToX(visibleArea.origin.x-person.frame.width, duration: time)
        
        self.worldNode.addChild(person)
        nPerson--
        print("Person spawned")
        
        //After move person remove it from Parent
        person.runAction(movePersonLeft, completion: {
            [unowned self] in
            
            if self.nPerson > 0 {
                self.spawnPerson(time)
            }else{
                self.voteCheck()
            }
            person.removeFromParent()
            print("Person died")
            
        })

    }

    // Give a Redish Color for 0 and Blueish Color for anyother
    func colorSelected(value: UInt32) -> SKSpriteNode{
        if value != 0 {
            nBlues++
            let blue = SKSpriteNode(texture: greekBlue.first!)
            let animate = SKAction.animateWithTextures(greekBlue, timePerFrame: 0.02)
            blue.runAction(SKAction.repeatActionForever(animate))
            return blue
        }
        nReds++
        let red = SKSpriteNode(texture: greekRed.first!)
        let animate = SKAction.animateWithTextures(greekRed, timePerFrame: 0.02)
        red.runAction(SKAction.repeatActionForever(animate))
        return red
    }
    
    
    func voteCheck(){
        
        createButtons()
        presentButtons()
    }
    
    func createButtons(){
        
        buttonView = SKSpriteNode(color: UIColor.blackColor().colorWithAlphaComponent(0.3), size: self.visibleArea.size)
        buttonView?.anchorPoint = CGPointZero
        buttonView?.position = self.visibleArea.origin
        buttonView?.zPosition = 9
        
        self.actionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: 1.4*CGRectGetMidY(self.visibleArea))
        self.actionLabel.removeAllChildren()
        self.actionLabel.text = "Qual é a maioria?"
        self.actionLabel.setShadow()
        self.actionLabel.alpha = 1
        
        redBT = Button(imageNamed: "empty", size: BUTTON_SIZE)
        redBT?.color = UIColor.redToga()
        redBT?.colorBlendFactor = 1
        redBT!.name = "redButton"
        redBT!.delegate = self
        
        blueBT = Button(imageNamed: "empty", size: BUTTON_SIZE)
        blueBT?.color = UIColor.blueToga()
        blueBT?.colorBlendFactor = 1
        blueBT!.name = "blueButton"
        blueBT!.delegate = self
    }
    
    func presentButtons() {
        
        let timeToShow = 0.3
        
        let spaceBetween = (self.frame.width - 2*BUTTON_WIDTH)/3.0
        
        redBT!.position = CGPointMake(spaceBetween + BUTTON_WIDTH/2, 0)
        blueBT!.position = CGPointMake(redBT!.position.x + BUTTON_WIDTH + spaceBetween, 0)
     
        let fadeIn = SKAction.fadeInWithDuration(2*timeToShow)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: 2*BUTTON_WIDTH), duration: timeToShow)
        let showUp = SKAction.group([fadeIn,moveUp])
        
        let nodesToAnimate = [redBT, blueBT]
        
        let containerNode = SKNode()
        containerNode.name = "HUDcontainer"
        containerNode.zPosition = 10
        for var i=0; i<nodesToAnimate.count; i++ {
            containerNode.addChild(nodesToAnimate[i]!)
        }
        self.worldNode.addChild(containerNode)
        self.worldNode.addChild(buttonView!)
        
        containerNode.runAction(showUp) { () -> Void in
            self.userInteractionEnabled = true
        }
    }
  
    override func getTimeFromDifficulty(difficulty:Difficulty) -> NSTimeInterval {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 17.0
        case (Difficulty.Easy):
            return 14.0
        case (Difficulty.Medium):
            return 12.0
        case (Difficulty.Hard):
            return 10.0
        case (Difficulty.VeryHard):
            return 9.0
            
        }
    }
}

extension Greece: SKPhysicsContactDelegate {
    func didBeginContact(contact: SKPhysicsContact) {}
}

extension Greece: ButtonDelegate{
    
    /** This treats what to do when buttons are tapped */
    func buttonTapped(button: Button){
        
        if nReds > nBlues && button.name! == "redButton"{
            
            self.minigameStatus = MinigameStatus.Success
            
        }else if nReds < nBlues && button.name! == "blueButton"{
            
            self.minigameStatus = MinigameStatus.Success
            
        }
        
        self.minigameHasEnded()
    }
}


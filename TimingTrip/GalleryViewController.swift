//
//  GalleryViewController.swift
//  TimingTrip
//
//  Created by Túlio Bazan da Silva on 16/12/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {
    
    @IBOutlet weak var galleryCollection: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var whiteLine: UIView!
    @IBOutlet weak var titleGallery: UILabel!
    @IBOutlet weak var titleContainerView: UIView!
    
    /** PopUp Struct with Transparent Black background view and the popUpView */
    struct popUpView {
        var bkgView: UIView? = nil
        var popUpView: GalleryPopUp? = nil
    }
    
    /** PopUpView Variable */
    var popUp: popUpView? = nil
    /** PopUp Check to don't open more than one popUp and Animation */
    var isPopUpPlaced: Bool = false
    /** List of information from JSON */
    var list = JSONParser.getList()
    let reuseIdentifier = "galleryCell" // also enter this string as the cell identifier in the storyboard
    
    
    // MARK: - Views and Layout
    
    /** Add popUp to view */
    func placePopUp(info: GalleryCard){
        self.isPopUpPlaced = true
        if self.popUp == nil{
            //First add a black panel with same frame as the view
            let panel = UIView.init(frame: self.view.bounds)
            panel.accessibilityIdentifier = "popUpPanel"
            panel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
            //Place popUp as child of the black panel and with 10 sized border
            let popUPFrame = CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.height-20)
            let popUp = GalleryPopUp.init(frame: popUPFrame, delegate: self, info: info)
            panel.addSubview(popUp)
            //Instantiate a new popUpView
            self.popUp = popUpView(bkgView: panel, popUpView: popUp)
        }
        self.popUp?.popUpView?.setInfo(info)
        self.view.addSubview((self.popUp?.bkgView)!)
    }
   
    /** Animate PopUp */
    func animatePopUp(){
        let timeToShow = 0.2
        //Opening Animation
        if isPopUpPlaced {
            let popFrame = CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.height-20)
            self.popUp?.popUpView?.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)
            UIView.animateWithDuration(timeToShow,
                delay: 0,
                options: [UIViewAnimationOptions.CurveEaseInOut],
                animations: {
                    self.popUp?.popUpView?.frame = popFrame
                
                }, completion: {(Bool) in
                    self.popUp?.popUpView?.scroolDescriptionUp()
            })
        //Closing Animation
        } else {
            let popFrame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)
            self.popUp?.popUpView?.frame = CGRectMake(10, 10, self.view.frame.size.width-20, self.view.frame.size.height-20)
            UIView.animateWithDuration(timeToShow,
                delay: 0,
                options: [UIViewAnimationOptions.CurveEaseInOut],
                animations: {
                    self.popUp?.popUpView?.frame = popFrame
                }, completion: {(Bool) in
                    self.popUp?.bkgView?.removeFromSuperview()
                })
        }
    }
    
    /** Make the FlowLayout of the Gallery */
    func galleryLayout(){
        //Round whiteLine border
        whiteLine.layer.cornerRadius = whiteLine.frame.size.width/2
        whiteLine.clipsToBounds = true

        //Text Title size
        titleGallery.font = titleGallery.font.fontWithSize(0.045*UIScreen.mainScreen().bounds.height)
        
        titleContainerView.layer.cornerRadius = 30
        titleContainerView.clipsToBounds = true

        
        //Setup FlowLayout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: self.view.frame.width * 0.3, left: 0, bottom: self.view.frame.width * 0.25, right: 0)
        layout.itemSize = CGSize(width: self.view.frame.width, height: self.view.frame.width * 0.3)
        layout.minimumInteritemSpacing = self.view.frame.width * 0.25
        layout.minimumLineSpacing = self.view.frame.width * 0.3
        //Set FlowLayout
        self.galleryCollection.collectionViewLayout = layout
    }
    
    // MARK: - Buttons Interactions
    //Back Button Action when released
    @IBAction func backClick(sender: AnyObject) {
        self.backBtn.releaseButton()
        self.dismissViewControllerAnimated(true, completion: {});
    }
    
   
    //Back Button Action when pressed
    @IBAction func buttonPressed(sender: AnyObject) {
        self.backBtn.pressButton()
    }
    
    // MARK: - Navigation
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup
        self.view.backgroundColor = UIColor.wallColor()
        self.galleryLayout()
        //add Shadow to the back button
        self.backBtn.setShadow()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Hides the Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

// MARK: - UICollectionViewDataSource
extension GalleryViewController:  UICollectionViewDataSource {

    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return self.items.count
        return self.list.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! GalleryCollectionViewCell
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.setTextOnLabel(list[indexPath.row].yearToString())
        cell.setPositionLabelAtSide(indexPath.row%2)
        cell.setImageNamed(list[indexPath.row].imageName)
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension GalleryViewController:  UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        switch indexPath.row {
        case 0:
            if(MedalsViewed.sharedInstance.medals[0] == 1) {
                MedalsViewed.sharedInstance.medals[0] = 2
            }
        case 1:
            if(MedalsViewed.sharedInstance.medals[1] == 1) {
                MedalsViewed.sharedInstance.medals[1] = 2
            }
        case 2:
            if(MedalsViewed.sharedInstance.medals[2] == 1) {
                MedalsViewed.sharedInstance.medals[2] = 2
            }
        default:
            break
        }
        //If has no popUpPlaced, place a popUp and Animate it
        if (!self.isPopUpPlaced) {
            self.placePopUp(self.list[indexPath.row])
            self.animatePopUp()
        }
    }
}

// MARK: - PopUpDelegate
extension GalleryViewController:  PopUpDelegate {
    
    //When the popUp is Closed this func is called
    func popClosed(sender: AnyObject) {
        //Remove popUpPanel from the superView
        self.isPopUpPlaced = false
        self.animatePopUp()
    }
    
}


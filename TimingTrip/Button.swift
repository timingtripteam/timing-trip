//
//  Button.swift
//  Timing Trip
//
//  Created by Lucas Mendonça on 9/30/15.
//  Copyright © 2015 Lucas Mendonça. All rights reserved.
//

import SpriteKit

protocol ButtonDelegate: class {
    func buttonTapped(node: Button)
}

class Button: SKSpriteNode {
    
    // MARK: - Declaration
    var enabledTexture: SKTexture
    var disabledTexture: SKTexture
    
    weak var delegate: ButtonDelegate? = nil
    
    var isEnabled: Bool = true
    var hasTwoTextures: Bool = false
    var isPressed: Bool = false
    
    init(imageNamed: String?, size: CGSize){
        
        let name: String = (imageNamed != nil) ? imageNamed! : "empty"
        
        enabledTexture = SKTexture(imageNamed: name)
        disabledTexture = SKTexture(imageNamed: name)
        
        super.init(texture: enabledTexture, color: UIColor.clearColor(), size: size)
        self.setShadow()
        self.userInteractionEnabled = true
    }
    
    convenience init(imageNamed: String?, otherImageNamed: String?, size: CGSize){
        self.init(imageNamed: imageNamed, size: size)

        disabledTexture = (otherImageNamed != nil) ? SKTexture(imageNamed: otherImageNamed!) : SKTexture(imageNamed: "empty")
        hasTwoTextures = true
    }
    
    /** This is required because Buttons can only be created programatically
    */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Tap Handling

    /** This is called when user has began touching this object
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        pushButton()
    }
    
    func pushButton() {
        if(!isPressed){
            isPressed = true
            self.position.y -= 0.1*self.size.height
            self.childNodeWithName("shadow")?.position.y += 0.1*self.size.height
            self.color = SKColor.blackColor()
            self.colorBlendFactor = 0.4
        }
    }
    /** This is called when the user touch was cancelled by some reason.
    Examples: a dialog box popping up, phone being blocked, app crashing, etc.
    */
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        releaseButton()
    }
    
    /** This is called when user has voluntarily stopped touching the object
    */
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        releaseButton()
        self.delegate?.buttonTapped(self)
    }
        
    /** This sets object back to original state whenever user is not touching it
    */
    func releaseButton() {
        if(self.isPressed){
            isPressed = false
            self.position.y += 0.1*self.size.height
            self.childNodeWithName("shadow")?.position.y -= 0.1*self.size.height
            self.color = SKColor.whiteColor()
            self.colorBlendFactor = 0.0
            
            if(self.hasTwoTextures){
                self.texture = isEnabled ? disabledTexture : enabledTexture
                isEnabled = !isEnabled
            }
        }
    }
    
}

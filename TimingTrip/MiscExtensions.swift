//
//  Extensions.swift
//  Timing Trip
//
//  Created by Lucas Mendonça on 8/11/15.
//  Copyright (c) 2015 Lucas Mendonça. All rights reserved.
//

import Foundation
import CoreGraphics
import SpriteKit
import UIKit
import GameplayKit
import AudioToolbox

// MARK: - Random Number Generator Extensions

/** Extends the Float type to enable random numbers
to be generated with a much visible range.
Usage: Float.random(10, to: 35)
*/
extension Float {
    static func random() -> Float {
        return Float(Float(arc4random()) / Float(UInt32.max))
    }
    static func random(start:Float, to:Float) -> Float {
        assert(start < to)
        return Float.random() * (to - start) + start

    }
}

/** Extends the CGFloat type to enable random numbers
 to be generated with a much visible range.
 Usage: CGFloat.random(10, to: 35)
 */extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(UInt32.max))
    }
    static func random(start:CGFloat, to:CGFloat) -> CGFloat {
        assert(start < to)
        return CGFloat.random() * (to - start) + start
    }
}

/** Extends the Int type to enable random numbers
 to be generated with a much visible range.
 Usage: Int.random(10, to: 35)
 */
extension Int {
    static func random(start:Int, to:Int) -> Int {
        return Int(arc4random_uniform(UInt32(to+1)) + UInt32(start))
    }
}

/** Extends the UIView type to enable:
 - Coping the UIView, getting a new one with the same attributes;
 - Adding Shadow to a UIView
 */
extension UIView {
    
    /** Coping the UIView, getting a new one with the same attributes */
    func copyView() -> AnyObject
    {
        return NSKeyedUnarchiver.unarchiveObjectWithData(NSKeyedArchiver.archivedDataWithRootObject(self))!
    }
    
    /** Add shadow to this UIView */
    func setShadow(){
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: self.bounds.size.height * 0.1)
        layer.shadowOpacity = 0.1
        layer.shadowRadius = 0
    }
}

/** Extends the UIButton type to enable:
 - press and release Button animation;
 */
extension UIButton {
    /** Moves button down, call it when pressed */
    func pressButton(){
        self.frame.origin.y += 0.1 * self.frame.size.height
        layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    /** Moves button up, call it when released */
    func releaseButton() {
        self.frame.origin.y -= 0.1 * self.frame.size.height
        layer.shadowOffset = CGSize(width: 0, height: self.frame.size.height * 0.1)
    }
   
}

/** New Animations - Prefab
 */
extension SKAction {
    static func fadeAfterDuration(alpha: Double, duration:Double) -> SKAction{
        let wait = SKAction.waitForDuration(duration*0.75)
        let fade = SKAction.fadeAlphaTo(0, duration: duration*0.25)
        
        return SKAction.sequence([wait, fade])
    }
    static func shake(duration: NSTimeInterval = 1, maxDeltaX: Int = 15, maxDeltaY: Int = 8, moveDuration: Double = 0.04) ->SKAction {
        //Number of movements
        let numberOfMoves = Int(duration/moveDuration)
        var moveActions: [SKAction] = []
        for _ in 1...numberOfMoves {
            //Gets the half a random delta
            let halfDeltaX = CGFloat(GKRandomSource.sharedRandom().nextIntWithUpperBound(maxDeltaX)) - CGFloat(maxDeltaX/2)
            let halfDeltaY = CGFloat(GKRandomSource.sharedRandom().nextIntWithUpperBound(maxDeltaY)) - CGFloat(maxDeltaY/2)
            //Each movement moves and goes back
            let moveGo = SKAction.moveByX(halfDeltaX, y: halfDeltaY, duration: moveDuration)
            let moveBack = moveGo.reversedAction()
            
            moveActions.append(moveGo)
            moveActions.append(moveBack)
        }
        return SKAction.sequence(moveActions)
    }
    
    static func vibrate() -> SKAction {
        return SKAction.runBlock({
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        })
    }
}


// FIXME: needs commentary
extension SKLabelNode {
    func setShadow(){
        let labelShadow = SKLabelNode(fontNamed:self.fontName)
        labelShadow.text = self.text
        labelShadow.fontSize = self.fontSize
        labelShadow.name = "\(self.name)Shadow"
        labelShadow.position = CGPoint(x: 0, y: -0.15*self.frame.height);
        labelShadow.fontColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        labelShadow.zPosition = -1
        self.addChild(labelShadow)
    }
}

extension SKSpriteNode {
    func setShadow(){
        let shadow = SKSpriteNode(texture: self.texture, size: self.size)
        shadow.name = "shadow"
        shadow.color = UIColor.blackColor().colorWithAlphaComponent(0.2)
        shadow.colorBlendFactor = 1.0
        shadow.zPosition = -1
        shadow.position = CGPointMake(self.position.x, self.position.y - 0.1*self.size.height)
        self.addChild(shadow)
    }
}



func shuffle <C: MutableCollectionType where C.Index.Distance == Int>(var list: C) -> C {
    var n = list.count
    if n == 0 { return list }
    let oneBeforeEnd = list.startIndex.advancedBy(n.predecessor())
    for i in list.startIndex..<oneBeforeEnd {
        let ran = Int(arc4random_uniform(UInt32(n--)))
        let j = i.advancedBy(ran)
        swap(&list[i], &list[j])
    }
    return list
}

/** This extension enables the SKNode to open a .sks file as a scene
*/
extension SKNode {
    class func unarchiveFromFile(file : String) -> SKScene? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            let sceneData = try! NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            let archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! SKScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

extension SKLabelNode {
    func multipleLineText() -> SKLabelNode {
        let subStrings:[String] = self.text!.componentsSeparatedByString("\n")
        var labelOutPut = SKLabelNode()
        var subStringNumber:Int = 0
        for subString in subStrings {
            
            
            
            let labelTemp = SKLabelNode(fontNamed: self.fontName)
            labelTemp.text = subString
            labelTemp.fontColor = self.fontColor
            labelTemp.fontSize = self.fontSize
            labelTemp.position = self.position
            labelTemp.zPosition = self.zPosition
            labelTemp.setShadow()
            labelTemp.horizontalAlignmentMode = self.horizontalAlignmentMode
            labelTemp.verticalAlignmentMode = self.verticalAlignmentMode
            
            let y:CGFloat = CGFloat(subStringNumber) * self.fontSize
            print("y is \(y)")
            if subStringNumber == 0 {
                labelOutPut = labelTemp
                subStringNumber++
            } else {
                labelTemp.position = CGPoint(x: 0, y: -y)
                labelOutPut.addChild(labelTemp)
                subStringNumber++
            }
        }
        return labelOutPut
    }
}


/** Optional Extension
 Usage: myOptionalVar.ifNil(defaultValue)
 */

extension Optional {
    public func ifNil(value: Wrapped) -> Wrapped {
        if let v = self {
            return v
        }
        return value
    }
}

func ifNil<T>(value:T?, use:T) -> T {
    if let v = value {
        return v
    }
    return use
}


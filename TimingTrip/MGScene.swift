//
//  MinigameScene.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 12/14/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit
import SpriteKit

protocol MGSceneDelegateProtocol: class {
    func minigameFinishedWithStatus(status:MinigameStatus)
}

/** This is the MiniGame Scene from which all minigames inherit
*/
class MGScene: SKScene {
    var minigameSceneDelegate: MGSceneDelegateProtocol?
    var minigameStatus = MinigameStatus.Failure

    var timerBar: MGTimerBar!
    var homeButton: Button!
    var resumeButton: Button!
    var pauseButton: Button!

    var pauseLabel = SKLabelNode(fontNamed:"Helvetica Neue Condensed Black")

    var actionText = ""
    var instructionText = ""
    
    // The big label that appears during the minigame
    var actionLabel = SKLabelNode(fontNamed: "Familiar Pro")
    // The small multiline label that appears during the minigame
    var instructionLabel = SKLabelNode(fontNamed:"Familiar Pro")
    
    var difficulty = Difficulty.Tutorial
    var pauseAnimationCompleted = true
    
    // All nodes who should be stopped by the pause
    var worldNode: SKNode!
    
    // MARK: Debug Variables
    
    var visibleArea: CGRect! // Rect with the visible area on the device
    var debugMode = false // Flag that indicates if debugMode is on or off
    
    // MARK: didMoveToView
    
    //Must be called on every minigame
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        
        // Gets the top right edge of the scene based on the view frame and device
        let topRightEdge = convertPointFromView(CGPointMake(view.frame.width,0))
        // Left top Edge
        let topLeftEdge = convertPointFromView(CGPointMake(0,0))
        // Rect with the visible area on the device
        visibleArea = CGRectMake(topLeftEdge.x, 0, topRightEdge.x - topLeftEdge.x, topRightEdge.y)
        
        //Create World Node if its not in sks file
        //Only the worldNode children will be paused
        if let node = childNodeWithName("WorldNode") {
            worldNode = node
        } else {
            worldNode = SKNode()
            worldNode.position = visibleArea.origin
            addChild(worldNode)
        }
        
        createPauseLabel()
        createHomeAndResumeButtons()
        createActionLabel()
        createInstructionLabel()
        createTimerBar(origin: topLeftEdge.x, topRightEdge: topRightEdge)
        createPauseButton(origin: topLeftEdge.x, topRightEdge: topRightEdge)
        
        if (debugMode) {
            configureSceneDebugMode()
        }

    }
    
    private func createHomeAndResumeButtons() {
        let size = CGSizeMake(0.15*visibleArea.width, 0.15*visibleArea.width)
        homeButton = Button(imageNamed: "home", size: size)
        homeButton.userInteractionEnabled = false
        homeButton.name = "homeButton"
        homeButton.zPosition = pauseLabel.zPosition
        homeButton.position = self.pauseLabel.position
        homeButton.position.y -= pauseLabel.frame.height/2 + homeButton.frame.height/2
        homeButton.position.x += homeButton!.frame.width
        homeButton.hidden = true

        self.addChild(homeButton)
        
        resumeButton = Button(imageNamed: "resumeButton", size: size)
        resumeButton.userInteractionEnabled = false
        resumeButton.name = "resumeButton"
        resumeButton.zPosition = pauseLabel.zPosition
        resumeButton.position = self.pauseLabel.position
        resumeButton.position.y -= pauseLabel.frame.height/2 + resumeButton.frame.height/2
        resumeButton.position.x -= resumeButton!.frame.width
        resumeButton.hidden = true
        self.addChild(resumeButton)
        
    }
    /// This configures the debugMode for the scene
    private func configureSceneDebugMode() {
        let lineWidth: CGFloat = 20
        let shapeArea = CGRectMake(visibleArea.origin.x+lineWidth/2, visibleArea.origin.y+lineWidth/2, visibleArea.width-lineWidth, visibleArea.height-lineWidth)
        let shape = SKShapeNode(rect: shapeArea)
        shape.strokeColor = UIColor.purpleColor()
        shape.lineWidth = lineWidth
        shape.zPosition = 20
        shape.position = CGPointZero
        addChild(shape)
    }
    
    /// This creates a TimerBar to represent time running out
    private func createTimerBar(origin originX: CGFloat, topRightEdge: CGPoint) {
        timerBar = MGTimerBar(originX: originX, topRightEdge: topRightEdge)
        timerBar.delegate = self
        
        //It will be paused
        self.worldNode.addChild(timerBar)
    }
    
    /// This creates a Pause Button to freeze minigames during gameplay
    private func createPauseButton(origin originX: CGFloat, topRightEdge: CGPoint) {
        let buttonWidth = 0.10*(topRightEdge.x-originX)
        let buttonSize = CGSizeMake(buttonWidth, buttonWidth)
        
        pauseButton = Button(imageNamed: "pause", size: buttonSize)
        pauseButton.name = "pauseButton"
        pauseButton.position = CGPointMake(topRightEdge.x-(0.75)*buttonWidth, topRightEdge.y-(0.75)*buttonWidth-self.timerBar.frame.height)
        pauseButton.zPosition = 50
        pauseButton.userInteractionEnabled = false
        
        self.addChild(pauseButton)
        
        let shadow = SKSpriteNode(imageNamed: "pauseShadow")
        shadow.name = "pauseShadow"
        shadow.position = pauseButton.position
        shadow.size = pauseButton.size
        shadow.zPosition = pauseButton.zPosition-1
        shadow.hidden = true
        self.addChild(shadow)
    }
    
    /// When a game has ended
    func minigameHasEnded() {
        timerBar?.removeAllActions()
        minigameSceneDelegate?.minigameFinishedWithStatus(minigameStatus)
    }
    
    // MARK: UI Control
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let location = touches.first?.locationInNode(self) {
            let nodes = nodesAtPoint(location)
            for node in nodes {
                if node.name == "pauseButton" {
                    pauseGame()
                }
                else if node.name == "resumeButton" {
                    unPauseGame()
                }
                else if node.name == "homeButton" {
                    (node as! Button).pushButton()
                }
                
            }
        }
    }
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        
        if worldNode.paused {
            if homeButton!.isPressed {
                homeButton!.releaseButton()
            }
            if resumeButton!.isPressed {
                homeButton!.releaseButton()
            }
        }
        
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        //Home button control
        if worldNode.paused {
            homeButton!.releaseButton()
            for touch in touches {
                let location = touch.locationInNode(self)
                let node = nodeAtPoint(location)
                if node.name == "homeButton" {
                    self.minigameSceneDelegate?.minigameFinishedWithStatus(.GameOver)
                }
            }
            if resumeButton!.isPressed {
                resumeButton.releaseButton()
            }
        }
        
    }
    
    internal func pauseAccelerometer() {
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.stopAccelerometerUpdates()
        }
    }
    internal func startAccelerometer() {
        
    }
    
    
    private func pauseGame() {
        //If the shadow animation has not ended
        if !pauseAnimationCompleted {
            return
        }
        homeButton?.hidden = false
        resumeButton?.hidden = false
        pauseAnimationCompleted = false
        
        pauseAccelerometer()
        //Pause physics
        physicsWorld.speed = 0
        self.pauseButton.hidden = true

        
        //Pauses the game
        self.worldNode.paused = !self.worldNode.paused
        
        //Shadow get bigger
        if let shadow = self.childNodeWithName("pauseShadow") {
            shadow.hidden = false
            shadow.runAction(SKAction.scaleTo( 2.5*visibleArea.height / pauseButton.frame.height, duration: 0.2), completion: {
                [unowned self] in
                self.pauseAnimationCompleted = true
            })
        }
        self.pauseLabel.hidden = false
    }
    
    func unPauseGame() {
        //If the shadow animation has not ended
        if !pauseAnimationCompleted {
            return
        }
        homeButton?.hidden = true
        resumeButton?.hidden = true

        pauseAnimationCompleted = false
        startAccelerometer()
        //Unpause physics
        physicsWorld.speed = 1
        
        pauseButton.hidden = false
        
        //Pauses the game
        self.worldNode.paused = !self.worldNode.paused
        
        if let shadow = self.childNodeWithName("pauseShadow") {
            shadow.runAction(SKAction.scaleTo(1, duration: 0.2), completion: {
                [unowned self] in
                self.pauseAnimationCompleted = true
            })
        }
        
        self.pauseLabel.hidden = true
    }
    
    func createPauseLabel(){
        self.pauseLabel.text = "PAUSADO";
        self.pauseLabel.fontSize = 0.15*self.visibleArea.width
        self.pauseLabel.name = "pauseLabel"
        self.pauseLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: CGRectGetMidY(self.visibleArea))
        self.pauseLabel.zPosition = 60
        self.pauseLabel.setShadow()
        self.pauseLabel.hidden = true
        self.addChild(self.pauseLabel)
    }
    
    func createActionLabel(){
        self.actionLabel.text = actionText
        self.actionLabel.fontSize = 0.05*self.visibleArea.height
        self.actionLabel.name = "actionLabel"
        self.actionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: 1.8*CGRectGetMidY(self.visibleArea))
        self.actionLabel.zPosition = 20
        self.actionLabel.setShadow()
        self.actionLabel.runAction(SKAction.fadeAfterDuration(0, duration: getTimeFromDifficulty(self.difficulty)/2))
        self.worldNode.addChild(self.actionLabel)
    }
    
    func createInstructionLabel(){
        self.instructionLabel.text = instructionText
        self.instructionLabel.fontSize = 0.03*self.visibleArea.height
        //self.instructionLabel.fontColor = UIColor.morroColor()
        self.instructionLabel.name = "tutorialLabel"
        self.instructionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: 1.7*CGRectGetMidY(self.visibleArea))
        self.instructionLabel.zPosition = 20
        self.instructionLabel.setShadow()
        self.instructionLabel = self.instructionLabel.multipleLineText()
        self.instructionLabel.runAction(SKAction.fadeAfterDuration(0, duration: getTimeFromDifficulty(self.difficulty)/2))
        self.worldNode.addChild(self.instructionLabel)
    }
    
    // MARK: Dificulty
    internal func getTimeFromDifficulty(difficulty:Difficulty) -> NSTimeInterval {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 10.0
        case (Difficulty.Easy):
            return 8.0
        case (Difficulty.Medium):
            return 6.0
        case (Difficulty.Hard):
            return 4.0
        case (Difficulty.VeryHard):
            return 3.0
        }
    }
    
}

// MARK: Timer control
extension MGScene: MinigameTimerDelegate {
    internal func timerHasFinished() {
        print("Timer has finished")
        minigameSceneDelegate?.minigameFinishedWithStatus(minigameStatus)
    }
}

// MARK: Load texture
extension MGScene {
    // Load the textures from an atlas and returns it
    internal func loadTexturesFromAtlasWithName(atlasName: String) -> [SKTexture] {
        let atlas = SKTextureAtlas(named: atlasName)
        var frames = [SKTexture]()
        
        for i in 1...atlas.textureNames.count {
            let fileName = "\(atlasName)\(i)"
            let texture = atlas.textureNamed(fileName)
            frames.append(texture)
        }
        return frames
    }
    
}
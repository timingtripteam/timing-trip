//
//  Transition.swift
//  Timing Trip
//
//  Created by Diego Silva de Carvalho on 28/09/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit
import SpriteKit

class TransitionScene: MGScene {
    
    var gameOver = false
    var score = 0
    var medalName:String!
    var medal: SKSpriteNode!
    var lock: SKSpriteNode!
    let transitionTime: NSTimeInterval = 4
    // Labels
    var scoreLabel: SKLabelNode!
    var gameOverLabel = SKLabelNode(fontNamed:"Helvetica Neue Condensed Black")

    // Buttons
    var shareButton: SKSpriteNode?
    var playButton: SKSpriteNode?
    var touchedButton: Button?
    // Animations
    var rings = [SKNode]()
    
    var BUTTON_WIDTH: CGFloat {
        return 0.15*self.visibleArea.width
    }
    var BUTTON_SIZE: CGSize {
        return CGSizeMake(BUTTON_WIDTH, BUTTON_WIDTH)
    }

    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)

        
        scoreLabel = self.worldNode.childNodeWithName("ScoreLabel") as! SKLabelNode
        timerBar.removeFromParent()
        if(gameOver) {
            self.worldNode.childNodeWithName("Player")!.removeFromParent()
            //remove pause
            self.childNodeWithName("pauseButton")?.removeFromParent()
            //hide rings
            let middleNode = self.worldNode.childNodeWithName("middleHole")!
            middleNode.removeFromParent()
            self.worldNode.childNodeWithName("Medal")!.removeFromParent()
            
            shareButton = Button(imageNamed: "share", size: BUTTON_SIZE)
            shareButton?.userInteractionEnabled = false
            shareButton?.zPosition = 40
            shareButton!.name = "ShareButton"
            playButton = Button(imageNamed: "home", size: BUTTON_SIZE)
            playButton?.userInteractionEnabled = false
            playButton!.name = "PlayAgainButton"
            playButton?.zPosition = 40
            createGameOverLabel()
            presentGameOverButtons()
            scoreLabel.text = String(score)

        } else {
            //
            configureMedal()
            startRingsAnimation()
            startPlayerFalling()
            addScore()
            self.worldNode.runAction(SKAction.waitForDuration(transitionTime), completion: {
                [unowned self] in
                self.minigameSceneDelegate?.minigameFinishedWithStatus(.NewGame)
            })
        }

    }
    
    func configureMedal() {
        medal = self.worldNode.childNodeWithName("Medal") as! SKSpriteNode
        medal.texture = SKTexture(imageNamed: medalName)
        lock = SKSpriteNode(imageNamed: "\(medalName)Locked")
        lock.size = medal.size
        lock.position = medal.position
        lock.zPosition = medal.zPosition+1
        self.worldNode.addChild(lock)
    }
    func showMedal() {
        let emitter = SKEmitterNode(fileNamed: "spark")!
        emitter.zPosition = lock.zPosition+1
        emitter.position = lock.position
        self.worldNode.addChild(emitter)
        lock.runAction(SKAction.fadeOutWithDuration(transitionTime/5), completion:{
            [unowned self] in
            self.lock.removeFromParent()
            
            self.medal.runAction(SKAction.scaleTo(1.1, duration: 0.2), completion: {
                [unowned self] in
                self.medal.runAction(SKAction.scaleTo(1, duration: 0.2), completion: {
                    [unowned self] in
                    self.medalFall()
                    emitter.removeFromParent()
                })
            })
        })
    }
    
    func medalFall() {
        let initialPos = worldNode.childNodeWithName("medalInitialPos")!
        let middlePos = worldNode.childNodeWithName("PlayerMiddlePos")!
        let finalPos = worldNode.childNodeWithName("PlayerFinalPos")!
        
        let time = transitionTime/5
        
        let moveToInitial = SKAction.moveTo(initialPos.position, duration: time/3)
        let scaleToInitial = SKAction.scaleTo(0.5, duration: time/3)
        let initialAction = SKAction.group([moveToInitial,scaleToInitial])
        
        let moveToMiddle = SKAction.moveTo(middlePos.position, duration: time/3)
        let scaleToMiddle = SKAction.scaleTo(0.3, duration: time/3)
        
        let middleAction = SKAction.group([moveToMiddle,scaleToMiddle])
        
        let moveToFinal = SKAction.moveTo(finalPos.position, duration: time/3)
        let scaleToFinal = SKAction.scaleTo(0.1, duration: time/3)
        
        
        let finalAction = SKAction.group([moveToFinal,scaleToFinal])
        medal.runAction(initialAction, completion: {
            [unowned self] in
            self.medal.runAction(middleAction, completion: {
                [unowned self] in
                self.medal.zPosition = 18
                self.medal.runAction(finalAction)
                })
        })
    }
    
    func startPlayerFalling() {
        let player = worldNode.childNodeWithName("Player")!
        let middlePos = worldNode.childNodeWithName("PlayerMiddlePos")!
        let finalPos = worldNode.childNodeWithName("PlayerFinalPos")!
        
        let time = transitionTime/3
        
        let moveToMiddle = SKAction.moveTo(middlePos.position, duration: time/2)
        let rotateToMiddle = SKAction.rotateToAngle(-0.87, duration: time/2)
        let scaleToMiddle = SKAction.scaleTo(0.6, duration: time/2)
        
        let initialAction = SKAction.group([moveToMiddle,rotateToMiddle,scaleToMiddle])
        
        let moveToFinal = SKAction.moveTo(finalPos.position, duration: time/2)
        let rotateToFinal = SKAction.rotateToAngle(-1.57, duration: time/2)
        let scaleToFinal = SKAction.scaleTo(0.1, duration: time/2)
        
        
        let finalAction = SKAction.group([moveToFinal,rotateToFinal,scaleToFinal])
        player.runAction(initialAction, completion: {
            [unowned self] in
            player.zPosition = 18
            self.showMedal()
            player.runAction(finalAction)
        })
        
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        for touch in touches {
            let location = touch.locationInNode(self)
            let node = nodeAtPoint(location)
            
            if (node.name == "ShareButton") {
                touchedButton = (node as! Button)
                touchedButton!.pushButton()
            }
            else if (node.name == "PlayAgainButton") {
                touchedButton = (node as! Button)
                touchedButton!.pushButton()
            }
            
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        
        if touchedButton != nil {
            touchedButton!.releaseButton()
            touchedButton = nil

        }
    
    }
    
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        if touchedButton != nil {
            touchedButton!.releaseButton()
            touchedButton = nil
        }
        
        for touch in touches {
            let location = touch.locationInNode(self)
            let node = nodeAtPoint(location)
            if (node.name == "ShareButton") {
                (node as! Button).releaseButton()
                self.share()
            }
            else if (node.name == "PlayAgainButton") {
                (node as! Button).releaseButton()
                self.minigameSceneDelegate?.minigameFinishedWithStatus(.GameOver)
            }
            
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        super.update(currentTime)
        
    }
    
    // Mark: Rings Animation
    func startRingsAnimation() {
        let middleNode = self.worldNode.childNodeWithName("middleHole")!
        for i in (0...5).reverse() {
            
            let node = middleNode.childNodeWithName("ring\(i)")!
            if(i != 5) {
                node.alpha = 0
            }
            rings.append(node)
        }
        let actions = getPosAndScaleActions()
        
        
        //reversed. Ring 0 = smallest ring
        let initialNode = self.rings[0]
        let initialPosition = initialNode.position
        let initialScale = initialNode.xScale
        let resetBlock = SKAction.customActionWithDuration(0.00001, actionBlock: {(actionNode,time) -> Void in
            actionNode.removeActionForKey("fade")
            actionNode.zPosition = 10
            actionNode.position = initialPosition
            actionNode.xScale = initialScale
            actionNode.yScale = initialScale
            actionNode.alpha = 1

        })
        
        for i in 1...rings.count-1 {
            rings[i].zPosition = 10
            rings[i].position = initialPosition
            rings[i].xScale = initialScale
            rings[i].yScale = initialScale
        }
        runAnimationOnRings(actions, resetBlock: resetBlock)
    }
    func runAnimationOnRings(animation: [SKAction], resetBlock: SKAction) {
        //Fade, scale and reset position
        //let fade = SKAction.fadeOutWithDuration(totalTime)
        //let block = SKAction.group([SKAction.sequence(animation),fade])
        let sequenceWithReset = SKAction.sequence([SKAction.sequence(animation),resetBlock])
        let completeAnimation = SKAction.repeatActionForever(sequenceWithReset)
        
        rings[0].runAction(completeAnimation)
        
        let waitTime = totalTime/Double(numberOfRings)
    
        for i in 1...rings.count-1 {

            rings[i].runAction(SKAction.waitForDuration(Double(i)*waitTime), completion:{
                [unowned self] in
                self.rings[i].alpha = 1
                self.rings[i].runAction(completeAnimation)
            })
        }
        
        
    }
    
    let totalTime = 1.5
    let numberOfRings: Double = 6

    func getPosAndScaleActions() -> [SKAction] {
        let time: NSTimeInterval = totalTime/Double(numberOfRings)
        
        var actions = [SKAction]()
        var i = 0
        for node in rings {
            let scaleAction = SKAction.scaleTo(node.xScale, duration: time)
            let moveAction = SKAction.moveTo(node.position, duration: time)
            if i == 3 {
                //Changes zPosition
                let block = SKAction.customActionWithDuration(0.00001, actionBlock: {(actionNode,time) -> Void in
                    actionNode.zPosition = 21
                    //let fadeTime = (ringsNumber-Double(i))*Double(time)
                    actionNode.runAction(SKAction.fadeOutWithDuration(1), withKey: "fade")
                })
                let group = SKAction.group([scaleAction, moveAction, block])
                actions.append(group)
            } else {
                let group = SKAction.group([scaleAction,moveAction])
                actions.append(group)
            }
            i++
            
            
        }
        
      
        
        return actions
    }
    
    func addScore() {
        //Shows last score
        let lastScore = score - 1
        scoreLabel.text = String(lastScore)
        //Add score with animation
        let scale = SKAction.scaleTo(1.5, duration: 0.5)
        scoreLabel.runAction(scale, completion: {
            [unowned self] in
            self.scoreLabel.text = String(lastScore+1)
            self.scoreLabel.runAction(SKAction.scaleTo(1.0, duration: 0.5))
        })
    }
    
    func presentGameOverButtons() {
        let timeToShow = 0.5
        
        let fadeIn = SKAction.fadeInWithDuration(timeToShow)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: 40), duration: timeToShow)
        
        let show = SKAction.group([fadeIn,moveUp])
        
        shareButton!.alpha = 0
        playButton!.alpha = 0
        shareButton!.position = CGPointMake(0.7*self.size.width, 0.14*self.size.height)
        playButton!.position = CGPointMake(0.3*self.size.width, 0.14*self.size.height)
        
        addChild(shareButton!)
        addChild(playButton!)
        
        shareButton!.runAction(show)
        playButton!.runAction(show)
        
    }
    
    func share() {
        let text = "Fiz \(score) pontos"
        let object = [text]
        if let rootVC = self.view?.window?.rootViewController {
            let activityVC = UIActivityViewController(activityItems: object, applicationActivities: nil)
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
                activityVC.popoverPresentationController!.sourceView = self.view
                let buttonPositionOnView = convertPointToView(CGPointMake(shareButton!.frame.origin.x+shareButton!.frame.width/2, shareButton!.frame.origin.y+shareButton!.frame.height+10))
                let rect = CGRectMake(buttonPositionOnView.x, buttonPositionOnView.y, 0, 0)
                activityVC.popoverPresentationController!.sourceRect = rect

            }
            
            
            activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
            
            rootVC.presentViewController(activityVC, animated: true, completion: nil)
        }
        
    }
    
    func createGameOverLabel(){
        self.gameOverLabel.text = "FIM DE JOGO";
        self.gameOverLabel.fontSize = 0.15*self.visibleArea.width
        self.gameOverLabel.name = "gameOverLabel"
        self.gameOverLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: CGRectGetMidY(self.visibleArea))
        self.gameOverLabel.zPosition = 60
        self.gameOverLabel.setShadow()
        self.addChild(self.gameOverLabel)
    }
}


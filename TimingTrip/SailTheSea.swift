//
//  SailTheSea.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 8/20/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import CoreMotion

class SailTheSea: MGScene, SKPhysicsContactDelegate {
    
    let playerCategory: UInt32    = 0x1 << 0;
    let rockCategory: UInt32      = 0x1 << 1;
    let boundsCategory: UInt32    = 0x1 << 2;
    let beachCategory: UInt32     = 0x1 << 3;

    var caravel = SKSpriteNode()
    var alreadyRotated:Bool = false
    var rock = SKSpriteNode()
    var t = NSTimeInterval()
    var beach = SKSpriteNode()
    
    var caravelTextures: [SKTexture]?
    var rockTextures: [SKTexture]?
    var beachTextures: [SKTexture]?
    
    var initialCaravelPos = CGPoint()
    
    var impulse:CGFloat = 0.0
    var movementAngle: CGFloat = 0.0

    
    override func didMoveToView(view: SKView) {
        self.actionText = "Guie a caravela"
        self.instructionText = "Incline o aparelho para \nchegar às Índias."
        super.didMoveToView(view)

        // time each rock takes to cross the screen
        let time = self.getTimeFromDifficulty(self.difficulty)/4
        
        let fade = SKAction.fadeAfterDuration(0, duration:time*0.5+1.5)
        var moveUp = SKAction.moveToY(CGRectGetMidY(self.visibleArea), duration: 1)
        var sequence = SKAction.sequence([moveUp,fade])

    
        self.actionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: -0.2*CGRectGetMidY(self.visibleArea))
        self.actionLabel.removeAllActions()
        self.actionLabel.runAction(sequence)
        
        moveUp = SKAction.moveToY(0.9*CGRectGetMidY(self.visibleArea), duration: 1)
        sequence = SKAction.sequence([moveUp,fade])
        
        self.instructionLabel.position = CGPoint(x:CGRectGetMidX(self.visibleArea), y: -0.3*CGRectGetMidY(self.visibleArea))
        self.instructionLabel.removeAllActions()
        self.instructionLabel.runAction(sequence)
        
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self
        //self.minigameStatus = MinigameStatus.Success
        createContent()
        
        setAccelerometer()
        timerBar.startTimerWith(getTimeFromDifficulty(difficulty))
        
        spawnRocks()
        
    }
    
  
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func createContent(){
        
        // Setting the scene bounds for collision
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect:visibleArea)
        self.physicsBody!.categoryBitMask = boundsCategory;
        self.physicsBody!.restitution = 0.0
        
        // Setting the background color
        self.backgroundColor = UIColor.sea().colorWithAlphaComponent(1.0)
        
        // Creating the scene objects
        createCaravel()
        createRock()
        createBeach()
    }
    
    override func didSimulatePhysics() {
        caravel.position.y = self.initialCaravelPos.y
    }
    
    func createCaravel(){
        let caravelFrameTime = 0.1
    
        caravelTextures = loadTexturesFromAtlasWithName("caravel")
        caravel = SKSpriteNode(texture: caravelTextures![0], size: CGSize(width: (self.visibleArea.height*0.45)*0.571, height: self.visibleArea.height*0.45))
        caravel.anchorPoint = CGPointMake(0.55, 0.3)
        
        caravel.position = CGPointMake(self.visibleArea.width/2, 0.75*self.visibleArea.height)
        initialCaravelPos = caravel.position
        
        caravel.zPosition = 5
        
        caravel.physicsBody = SKPhysicsBody(circleOfRadius: 0.2*caravel.frame.width)
        caravel.physicsBody?.dynamic = true
        caravel.physicsBody?.restitution = 0.0
        caravel.physicsBody?.affectedByGravity = false
        
        self.worldNode.addChild(caravel)
        
        caravel.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(caravelTextures!, timePerFrame: caravelFrameTime, resize: false, restore: true)))
    }
    
    func createRock() {
        let rockFrameTime = 0.25
        
        rockTextures = loadTexturesFromAtlasWithName("rock")
        rock = SKSpriteNode(texture: rockTextures![0], size: CGSize(width: self.visibleArea.width*0.3, height: self.visibleArea.width*0.3*0.783))
        rock.anchorPoint = CGPointMake(0.5, 0.5)
        rock.physicsBody = SKPhysicsBody(circleOfRadius: rock.frame.height/2)
        rock.physicsBody?.dynamic = true
        //rock.physicsBody?.restitution = 0.0
        rock.physicsBody?.affectedByGravity = false
        rock.zPosition = 2
        
        rock.physicsBody?.categoryBitMask = rockCategory
        rock.physicsBody?.contactTestBitMask = playerCategory
        rock.physicsBody?.collisionBitMask = 0
        
        self.worldNode.addChild(rock)
        
        let rockPosition = self.visibleArea.origin.x + CGFloat.random(0.3*self.self.visibleArea.width, to: 0.7*self.visibleArea.width)
        rock.position = CGPointMake(rockPosition, -rock.frame.height)
        
        rock.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(rockTextures!, timePerFrame: rockFrameTime, resize: false, restore: true)))
    }
    
    func createBeach() {
        let beachFrameTime = 0.2
        
        beachTextures = loadTexturesFromAtlasWithName("island")
        beach = SKSpriteNode(texture: beachTextures![0])
        beach.anchorPoint = CGPointMake(0.5, 1.0)
        beach.setScale(self.visibleArea.width/self.beach.frame.width)
        self.worldNode.addChild(beach)
        beach.position = CGPointMake(CGRectGetMidX(visibleArea), -beach.frame.height)
        beach.zPosition = -10
        beach.physicsBody?.categoryBitMask = beachCategory
        beach.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(beachTextures!, timePerFrame: beachFrameTime, resize: false, restore: true)))
        
        //Grass View
        let height = visibleArea.height * 2
        let width = visibleArea.width * 10
        let view = SKSpriteNode(color: UIColor.islandGrassColor(), size: CGSize(width: width, height: height))
        view.zPosition = -10
        view.position = CGPointMake(0, -beach.frame.height - view.frame.height)
        beach.addChild(view)
    }
    
    
    override func startAccelerometer() {
        super.startAccelerometer()
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.startAccelerometerUpdatesToQueue(NSOperationQueue(), withHandler: {
                data, error in
                
                if data!.acceleration.x < 0 {
                    self.impulse = CGFloat(data!.acceleration.x*2)
                    self.movementAngle = CGFloat(-0.05*M_PI)
                }
                    
                else if data!.acceleration.x > 0 {
                    self.impulse = CGFloat(data!.acceleration.x*2)
                    self.movementAngle = CGFloat(0.05*M_PI)
                }
                    
                else if data!.acceleration.x == 0 {
                    self.impulse = 0
                    self.movementAngle = 0
                }
                
                self.caravel.physicsBody?.applyImpulse(CGVectorMake(self.impulse, 0))
                
                let vx = abs(self.caravel.physicsBody!.velocity.dx)
                
                if(vx > 350){
                    self.caravel.runAction(SKAction.rotateToAngle(self.movementAngle, duration:0.2))
                }else if(vx < 100 && (self.caravel.position.x > 0.1*self.visibleArea.width) &&
                    (self.caravel.position.x < 0.9*self.visibleArea.width)){
                        self.caravel.runAction(SKAction.rotateToAngle(0, duration:0.2))
                }
                
                //print("caravel Pos = \(100*self.caravel.position.x/self.frame.width)")
            })
        }

    }
    func setAccelerometer(){
        startAccelerometer()
    }
    
    func spawnRocks(){
        
        // number of times the rock spawns
        let n:Float = 4.0
        
        // time each rock takes to cross the screen
        t = NSTimeInterval(Float(getTimeFromDifficulty(difficulty))/(n+3))
        
//        print("dificuldade: \(self.difficulty)")
//        print("tempo: \(getTimeFromDifficulty(difficulty))")
//        print("t (pedra): \(t)")
        
        for _ in 0...2 {
            let posY = -2.0*self.rock.frame.height
            var posX = visibleArea.origin.x+CGFloat.random(0.35*visibleArea.width, to: 0.65*visibleArea.width)
            
            let moveRockUp = SKAction.moveToY(visibleArea.height + 2*self.rock.frame.height, duration: t)
            let wait = SKAction.waitForDuration(t/Double(2*n+2))
            let moveRockUpThenWait = SKAction.sequence([moveRockUp, wait])
            
            rock.runAction(SKAction.waitForDuration(3.5), completion: { [unowned self] in
                self.rock.runAction(moveRockUpThenWait, completion: { [unowned self] in
                    // Resetting the rock's position and angle
                    posX = CGFloat.random(0.1*self.visibleArea.width, to: 0.9*self.visibleArea.width)
                    self.rock.position = CGPointMake(posX, posY)
                    self.rock.zRotation = 0.0
                    
                    // Spawning the second rock
                    self.rock.runAction(moveRockUpThenWait, completion: { [unowned self] in
                        
                        // Resetting the rock's position one more time
                        self.rock.position = CGPointMake(posX, posY)
                        self.rock.zRotation = 0.0
                        
                        // Spawning the third rock
                        self.rock.runAction(moveRockUpThenWait, completion: { [unowned self] in
                            
                            // Resetting the rock's position one more time
                            self.rock.position = CGPointMake(posX, posY)
                            self.rock.zRotation = 0.0
                            
                            // Spawning the fourth rock
                            self.rock.runAction(moveRockUpThenWait, completion: { [unowned self] in
                                
                                // If player has survived all four rocks
                                if(self.minigameStatus == MinigameStatus.Success){
                                    self.spawnBeach(n)
                                }
                            })
                        })
                        
                    })
                })

            })
        }
    }
    
    func spawnBeach(n: Float){
        let moveBeachUp = SKAction.moveToY(self.caravel.position.y + self.caravel.frame.height/3, duration: 1.3*self.t)
        self.beach.runAction(moveBeachUp, completion: { () -> Void in
            self.runAction(SKAction.waitForDuration(self.t/Double(2*n+2)), completion: { () -> Void in
                self.pauseAccelerometer()
                self.minigameHasEnded()
            })
        })
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        minigameStatus = MinigameStatus.Failure
        
        // If we need to verify which object has collided with the player, use this code
        /*if((contact.bodyA.categoryBitMask == rockCategory) && ( contact.bodyB.categoryBitMask == playerCategory)) {
            //code here
        }*/
        rock.physicsBody = nil
        
        let collisionTime = NSTimeInterval((caravel.position.y - rock.position.y)/(self.visibleArea.height/CGFloat(self.t)))
        
        rock.runAction(SKAction.moveToY(caravel.position.y, duration: collisionTime), completion: { () -> Void in
            self.rock.removeAllActions()
            self.caravel.runAction(SKAction.shake())
            self.rock.runAction(SKAction.shake())
            self.runAction(SKAction.vibrate())
            
            let rotate = SKAction.rotateByAngle(CGFloat(M_PI/4), duration:1.0)
            let fade = SKAction.fadeAlphaTo(0.0, duration: 1.0)
            let darken = SKAction.colorizeWithColor(UIColor.blackColor().colorWithAlphaComponent(0.2), colorBlendFactor: 0.8, duration: 0.2)
            
            let group = SKAction.group([rotate, fade, darken])
            
            self.rock.runAction(SKAction.group([fade,darken]))
            self.caravel.runAction(group, completion: { () -> Void in
                self.minigameHasEnded()
            })
        })
    }
    
    override func getTimeFromDifficulty(difficulty:Difficulty) -> NSTimeInterval {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 32.0
        case (Difficulty.Easy):
            return 26.0
        case (Difficulty.Medium):
            return 22.0
        case (Difficulty.Hard):
            return 20.0
        case (Difficulty.VeryHard):
            return 11.0
        
        }
    }
    
    
}

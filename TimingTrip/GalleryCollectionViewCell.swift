//
//  GalleryCollectionViewCell.swift
//  TimingTrip
//
//  Created by Túlio Bazan da Silva on 16/12/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    //Gallery
    @IBOutlet weak var lblRight: UILabel!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var imgPreview: UIImageView!
    
    /** If side equal to 0 position is Left other wise position is Right*/
    func setPositionLabelAtSide(side: Int){
        // if Left
        if side == 0 {
            lblRight.hidden = true
            lblLeft.hidden = false
        } else {
            lblRight.hidden = false
            lblLeft.hidden = true
        }
    }
    
    func setTextOnLabel(label: String){
        lblLeft.text = label
        lblLeft.font = lblLeft.font.fontWithSize(0.0375*UIScreen.mainScreen().bounds.height)
        lblLeft.shadowOffset = CGSizeMake(0.0,  0.08*self.lblLeft.frame.height)
        
        lblRight.text = label
        lblRight.font = lblRight.font.fontWithSize(0.0375*UIScreen.mainScreen().bounds.height)
        lblRight.shadowOffset = CGSizeMake(0.0,  0.08*self.lblRight.frame.height)
    }
    
    func setImageNamed(named: String){
        imgPreview.layer.borderWidth = 5
        imgPreview.layer.cornerRadius = self.frame.size.height/2
        imgPreview.clipsToBounds = true
        imgPreview.image = UIImage(named: named)
        
        imgPreview.layer.borderColor = UIColor.whiteColor().CGColor
    }
}

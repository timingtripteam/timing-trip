//
//  Score.swift
//  Timing Trip
//
//  Created by Lucas Mendonça on 9/21/15.
//  Copyright (c) 2015 Lucas Mendonça. All rights reserved.
//

import UIKit

/** This class stores the game score 
*/
class Score: NSObject {
    static let sharedInstance = Score()
    var value: Int = 0
}

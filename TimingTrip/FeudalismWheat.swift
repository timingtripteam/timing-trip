//
//  FeudalismWheat.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 27/12/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import SpriteKit
import UIKit

enum FeudalismWheatPhysics:UInt32 {
    case None = 0
    case Wheat = 1
    case Basket = 2
}

class FeudalismWheat: MGScene {
    
    var basket: SKSpriteNode!
    var wheatCounter = 0
    var touchedNode: SKNode?
    var cloudArray = [SKSpriteNode]()
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        physicsWorld.contactDelegate = self
        configurePhysics()
        configureClouds()
        
        timerBar.startTimerWith(getTimeFromDifficulty(difficulty))
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        
        guard let touchLocation = touches.first?.locationInNode(worldNode) else {
            return
        }
        
        let nodes = worldNode.nodesAtPoint(touchLocation)
        for node in nodes {
            if node.name == "Wheat" {
                touchedNode = node
            }
        }
        
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        guard let previousLocation = touches.first?.previousLocationInNode(worldNode) else {
            return
        }
        guard let touchLocation = touches.first?.locationInNode(worldNode) else {
            return
        }
        
        if touchedNode != nil {
            touchedNode!.position.x += touchLocation.x - previousLocation.x
            touchedNode!.position.y += touchLocation.y - previousLocation.y

        }
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        touchedNode = nil
        
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        touchedNode = nil

    }
    
    override func didSimulatePhysics() {
        super.didSimulatePhysics()
        
        for cloud in cloudArray {
            if cloud.position.x < visibleArea.origin.x {
                cloud.position.x = visibleArea.maxX+cloud.frame.width
            }
        }
        
    }
    func configureClouds() {
        guard let clouds = self.worldNode.childNodeWithName("//CloudNode") else {
            fatalError("No Clouds found")
        }
        
        var cloudVelocity = CGVectorMake(-100, 0)
        for cloud in clouds.children {
            //Set initial position
            let position = arc4random_uniform(UInt32(visibleArea.width))
            cloud.position.x = CGFloat(position) + visibleArea.origin.x
            //Configure velocity
            let cloudBody = SKPhysicsBody(rectangleOfSize: (cloud as! SKSpriteNode).size)
            cloudBody.allowsRotation = false
            cloudBody.affectedByGravity = false
            cloudBody.categoryBitMask = FeudalismWheatPhysics.None.rawValue
            cloudBody.collisionBitMask = FeudalismWheatPhysics.None.rawValue
            cloudBody.contactTestBitMask = FeudalismWheatPhysics.None.rawValue
            cloud.physicsBody = cloudBody
            cloud.physicsBody!.velocity = cloudVelocity
            cloudVelocity.dx *= 2
            cloudArray.append((cloud as! SKSpriteNode))
        }
        
    }
    func configurePhysics() {
        guard let wheatNode = self.worldNode.childNodeWithName("WheatNode") else {
            fatalError("No Wheat Node found")
        }
        guard let basketNode = self.worldNode.childNodeWithName("//WheatBag") as? SKSpriteNode else {
            fatalError("No Wheat Bag found")
        }
        basket = basketNode
        
        for node in wheatNode.children {
            node.physicsBody = getWheatPhysicsBody(node as! SKSpriteNode)
        }
        
        let basketBody = SKPhysicsBody(rectangleOfSize: basket.size)
        basketBody.allowsRotation = false
        basketBody.affectedByGravity = false
        basketBody.categoryBitMask = FeudalismWheatPhysics.Basket.rawValue
        basketBody.collisionBitMask = FeudalismWheatPhysics.None.rawValue
        basketBody.contactTestBitMask = FeudalismWheatPhysics.None.rawValue
        basket.physicsBody = basketBody
        
        
    }
    
    func getWheatPhysicsBody(node: SKSpriteNode) ->SKPhysicsBody {
        let body = SKPhysicsBody(rectangleOfSize: node.size)
        body.allowsRotation = false
        body.affectedByGravity = false
        body.categoryBitMask = FeudalismWheatPhysics.Wheat.rawValue
        body.collisionBitMask = FeudalismWheatPhysics.None.rawValue
        body.contactTestBitMask = FeudalismWheatPhysics.Basket.rawValue
        return body
    }
    
    func handleWheatAndBasketCollision(bodyA: SKPhysicsBody, bodyB: SKPhysicsBody) {
        guard let nodeA = bodyA.node, let nodeB = bodyB.node else {
            return
        }
        if nodeA == basket {
            nodeB.removeFromParent()
        }
        else {
            nodeA.removeFromParent()
        }
        
        let scaleSequence = SKAction.sequence([SKAction.scaleTo(1.1, duration: 0.3), SKAction.scaleTo(1, duration: 0.3)])
        basket.runAction(scaleSequence, completion:  {
            [unowned self] in
            self.wheatCounter++
            if self.wheatCounter == 4 {
                self.minigameStatus = .Success
                self.minigameHasEnded()
            }
        })
    }
    
}

extension FeudalismWheat: SKPhysicsContactDelegate {
    func didBeginContact(contact: SKPhysicsContact) {
        let collision = contact.bodyA.categoryBitMask|contact.bodyB.categoryBitMask
        
        let wheatAndBasket = FeudalismWheatPhysics.Wheat.rawValue|FeudalismWheatPhysics.Basket.rawValue
        
        
        if collision == wheatAndBasket {
            handleWheatAndBasketCollision(contact.bodyA, bodyB: contact.bodyB)
        }
    }
    
    
    
}


//
//  GameController.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 12/14/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion


protocol DebugDelegate {
    func changeDebug()
}

class GameController: UIViewController {

    var debugMode = false
    var score = 0
    var lives = 1
    var lastMinigameType: MiniGameType?
    // This gets called only when the game starts for the first time
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentMainMenu()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        var newMedals = 0
        for number in MedalsViewed.sharedInstance.medals {
            if number == 1 {
                newMedals++
            }
        }
        if let skView = view as? SKView {
            if let start = skView.scene as? StartScene {
                start.newMedals = newMedals
            }
        }
        
    }
    
    // MARK: Next Minigame

    // Gets a random minigame and presents it
    func presentNextMinigame() {
        
        var nextMinigame = MiniGameType.onSequence()
        
        while lastMinigameType == nextMinigame {
            nextMinigame = MiniGameType.onSequence()
        }
        lastMinigameType = nextMinigame
        
        
        // Uncomment this line to test minigames
        


        
        switch (nextMinigame) {

        case .SailTheSea:
            let nextScene = SailTheSea(size: view.bounds.size)
            nextScene.difficulty = .Medium

            presentNextScene(nextScene)
            break
            
        case .Feudalism:
            let nextScene = Feudalism(fileNamed: "Feudalism")!
            nextScene.difficulty = .Easy

            presentNextScene(nextScene)
            break
            
        case .Greece:
            let nextScene = Greece(fileNamed: "Greece")!
            nextScene.difficulty = .Easy

            presentNextScene(nextScene)
            break
            
        default:
            print("ERROR: At GameController.swift: \(nextMinigame) was not found")
            break
        }
    }
    
    //MARK: Main Menu
    func presentMainMenu() {
        var newMedals = 0
        //0 = locked, 1 = unlocked and not visited, 2 = unlocked and visited
        for number in MedalsViewed.sharedInstance.medals {
            if number == 1 {
                newMedals++
            }
        }
        let nextScene = StartScene(size: view.bounds.size)
        nextScene.newMedals = newMedals
        nextScene.minigameScebeDelegate = self
        nextScene.debugDelegate = self
        presentNextScene(nextScene)
    }
    
    //MARK: Transition Scene
    func presentTransitionScene() {
        var medal = ""
        switch (lastMinigameType!) {
        case .SailTheSea:
            medal = "seaMedal"
        case .Feudalism:
            medal = "feudalismMedal"
        case .Greece:
            medal = "greeceMedal"
        default:
            medal = "no medal"

        }
        
        
        if let nextScene = TransitionScene.unarchiveFromFile("TransitionScene") as? TransitionScene{
            nextScene.score = score
            nextScene.medalName = medal
            nextScene.minigameSceneDelegate = self
            if (lives == 0) {
                //MARK: Gameover
                //TODO: implementar o gameover
                nextScene.gameOver = true
                
            }
            presentNextScene(nextScene)
        } else {
            fatalError("No transition scene found")
        }
        
    }
    
    ///Configure and presents the nextScene
    func presentNextScene(scene: SKScene) {
        //Configure Scene
        scene.scaleMode = .AspectFill
        
        //minigameScene delegate is responsible for the comunication between the minigames and the controller
        if scene is MGScene {
            (scene as! MGScene).minigameSceneDelegate = self
            
        }
        
        //Configure SKView
        let skView = view as! SKView
        
        //Configure the debug mode
        if (debugMode) {
            if scene is MGScene {
                (scene as! MGScene).debugMode = true
                
            }
            configureDebugMode(skView)
        }
        
        //Must set zPosition properly!
        skView.ignoresSiblingOrder = true
        
        //Present scene
        skView.presentScene(scene)
    }
    
    ///Debug mode
    func configureDebugMode(skView: SKView) {
        skView.showsPhysics = true
        skView.showsNodeCount = true
        skView.showsFPS = true
       
    }
    
    // Hides the Status Bar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    func medalUnlock() {
        switch (lastMinigameType!) {
        case .Greece:
            
            if (MedalsViewed.sharedInstance.medals[0] == 0) {
                MedalsViewed.sharedInstance.medals[0] = 1
            }
        case .Feudalism:
            if (MedalsViewed.sharedInstance.medals[1] == 0) {
                MedalsViewed.sharedInstance.medals[1] = 1
            }
        case .SailTheSea:
            if (MedalsViewed.sharedInstance.medals[2] == 0) {
                MedalsViewed.sharedInstance.medals[2] = 1
            }
            
        }
    }
}

//MARK: Minigame Scene Delegate Protocol

///Called every time a minigame finishes
extension GameController: MGSceneDelegateProtocol {
    func minigameFinishedWithStatus(status: MinigameStatus) {
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.stopAccelerometerUpdates()
        }
        switch(status) {
        case .Success:
            medalUnlock()
            score++
            presentTransitionScene()
            
        case .Failure:
            lives--;
            presentTransitionScene()

        case .GameOver:
            lives = 1
            presentMainMenu()
        case .NewGame:

            presentNextMinigame()
        }

    }
}

extension GameController: DebugDelegate {
    func changeDebug() {
        if (debugMode) {
            debugMode = false
        } else {
            debugMode = true
        }
    }
}

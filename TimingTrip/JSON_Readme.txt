/********************************************************************************************************

How to Use:

Cardinfos will be displayed at GalleryViewController, when opened a GalleryPopUp. It will give information
about a time to the user. The popUp will show information like bellow:

___________________________
|                         |
|                         |
|                         |
|                         |
|       imageName         |
|                         |
|                         |
|                         |
|                         |
|_________________________|
|    CardName - year      |
|_________________________|
|                         |
|                         |
|                         |
|       description       |
|                         |
|                         |
|                         |
|                         |
|                         |
---------------------------

To add a new item to "cardInfos", following the structure below:

{
"cardName": "Write Here the card name",
"year": -1000, //Negative to a.C. and Positive to d.C. years
"description": "Write here the description. If needed put \n after a Paragraph, to write a breakline.",
"imageName": "The name of the image to display"
},

- No need to add in chronological order.

**********************************************************************************************************

Example:
{
"cardName": "Grécia",
"year": -500,
"description": "Em 508 a.C. foi criado na cidade de Atenas um novo sistema político - a democracia - que representava uma alternativa à tirania. O cidadão ateniense Clístenes propôs algumas reformas que concediam a cada cidadão um voto apenas, nas assembleias regulares relativas a assuntos públicos.\n\nEsta alternativa à tirania incluía camponeses, mas excluía as mulheres como iguais. No entanto, como experiência política seria a mais imitada e copiada de todas.\n\nTodos os cidadãos do sexo masculino eram livres de assistir às assembleias, que debatiam e ratificavam as questões civis, normalmente quatro vezes por mês.\n\nNão havia nesse tempo partidos políticos organizados; contrariamente aos sistemas democráticos atuais, a democracia grega não se regia pela eleição dos representantes, as decisões respeitavam sim a opinião da maioria relativamente a cada assunto aberto ao debate.",
"imageName": "FeudalismBG"
},

**********************************************************************************************************
*/
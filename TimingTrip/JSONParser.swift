//
//  JSONParser.swift
//  TimingTrip
//
//  Created by Túlio Bazan da Silva on 05/01/16.
//  Copyright © 2016 TimingTripTeam. All rights reserved.
//

import UIKit

struct GalleryCard {
    
    var cardName = ""
    var year : Double = 0
    var description = ""
    var imageName = ""
    var url = ""
    
    //Make the year Int value to a String
    func yearToString() -> String{
        var yearSTR = ""
        let format = String(format:"%.0f", fabs(year))
        
        if self.year < 0 {
            yearSTR = "\(format) a.C."
        } else {
            yearSTR = "\(format) d.C."
            
        }
        return yearSTR
    }
    
}

class JSONParser: NSObject {
    
    //Struct of a Item from the JSON
    
    
    //Give a list of all Structs from the JSON - galleryJSON, ordered by year
    static func getList() -> Array<GalleryCard> {
       
        let list = parseJSON(dataToObject(readJSON("galleryJSON")))
        let sorted = list.sort({ $0.year < $1.year})
        
        return sorted
        
    }
    
    //Get the JSON file and make a NSData from it
    static func readJSON(fileName: String) -> NSData{
        
        let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "json")
        let fileUrl = NSURL(fileURLWithPath: path!)
        
        var jsonData : NSData!
        
        do {
            jsonData = try NSData(contentsOfURL: fileUrl, options: .DataReadingMappedIfSafe)
        } catch {
            print(error)
        }
        
        return jsonData!
    }
    
    //Get a NSData of a JSON and convert to a AnyObject
    static func dataToObject(data: NSData) -> AnyObject{

        var anyObj: AnyObject?
        
        do {
            anyObj = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
        } catch {
            print(error)
        }

        return anyObj!
    }
    
    //Take a Parsed JSON Object and Convert to a list of a Struct
    static func parseJSON(anyObj:AnyObject) -> Array<GalleryCard>{
        
        var list:Array<GalleryCard> = []
        
        if anyObj is Dictionary<String,AnyObject>{
            
            let dict = anyObj as! Dictionary<String,AnyObject> //Convert the AnyObject to a Dictionary
            
            let cards = dict["cardInfos"] as! Array<AnyObject> //Convert the AnyObject to a Array
            
            for json in cards{
                
                let jsond = json as! Dictionary<String,AnyObject> //Convert the AnyObject item to a Dictionary
                
                var b: GalleryCard = GalleryCard() //Create a Object from the Struct, and populate it
                
                b.cardName = (jsond["cardName"] as AnyObject? as? String) ?? "" // to get rid of null
                b.description = (jsond["description"] as AnyObject? as? String) ?? ""
                b.imageName = (jsond["imageName"] as AnyObject? as? String) ?? ""
                b.year  =  (jsond["year"]  as AnyObject? as? Double) ?? 0
                b.url = (jsond["url"] as AnyObject? as? String) ?? ""
                
                list.append(b)
            }

        }
        
        return list
        
    }
    
}

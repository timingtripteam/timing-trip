//
//  MGTimer.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 12/17/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import SpriteKit

import UIKit
import SpriteKit

protocol MinigameTimerDelegate: class {
    func timerHasFinished()
}

/** This represents the Minigame Timer Bar that appears at the top during MiniGames.
    Its child, the Highlighted Bar, is the one who moves to the left as time passes.
 */
class MGTimerBar: SKSpriteNode {
    
    let Z_POSITION: CGFloat = 200

    weak var delegate: MinigameTimerDelegate?
    var highlightBar: SKSpriteNode!
    
    convenience init(originX: CGFloat, topRightEdge: CGPoint) {
        let timerSize = CGSizeMake(topRightEdge.x - originX, 0.05*topRightEdge.x)
        
        let barPosition = CGPointMake(originX, topRightEdge.y)
        let newAnchorPoint = CGPointMake(0, 1)
        let shadowColor = UIColor.blackColor().colorWithAlphaComponent(0.2)

        self.init(color: shadowColor, size: timerSize)
        
        // Set the timer's shadow and background
        self.position = barPosition
        self.zPosition = Z_POSITION
        self.anchorPoint = newAnchorPoint
        
        // Set the Highlight Bar (the one that moves)
        highlightBar = SKSpriteNode(color: UIColor.whiteColor(), size: timerSize)
        highlightBar.position = CGPointZero
        highlightBar.zPosition = 1
        highlightBar.anchorPoint = newAnchorPoint
        addChild(highlightBar)
    }
    
    func startTimerWith(duration: NSTimeInterval) {
        let moveLeft = SKAction.moveByX(-self.size.width, y: 0, duration: duration)
        highlightBar.runAction(moveLeft, completion: {
            [unowned self] in
            self.delegate?.timerHasFinished()
        })
    }
    
}

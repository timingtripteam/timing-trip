//
//  GalleryPopUp.swift
//  TimingTrip
//
//  Created by Túlio Bazan da Silva on 21/12/15.
//  Copyright © 2015 TimingTripTeam. All rights reserved.
//

import UIKit


protocol PopUpDelegate {
    func popClosed(sender: AnyObject)
    
}
@IBDesignable class GalleryPopUp: UIView {
    
    /** Image Displayed at PopUp */
    @IBOutlet weak var displayImg: UIImageView!
    /** Year Label*/
    @IBOutlet weak var yearLbl: UILabel!
    /** Text description about the pop up*/
    @IBOutlet weak var descriptionText: UITextView!
    /** Close button popup */
    @IBOutlet weak var closeBtn: UIButton!
    
    var delegate: PopUpDelegate? = nil
    var view : UIView!
    
    convenience init(frame: CGRect, delegate: PopUpDelegate, info: GalleryCard){
        self.init(frame: frame)
        self.delegate = delegate
        setInfo(info)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup(){
        
        //View Setup
        self.view = loadViewFromNib()
        self.view.frame = bounds
        self.view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.view.layer.cornerRadius = 30
        self.view.clipsToBounds = true
        
        //Background Color of views
        self.view.backgroundColor = UIColor.doorColor()
        self.displayImg.backgroundColor = UIColor.doorColor()
        self.descriptionText.backgroundColor = UIColor.doorColor()
        
        //Close Button Setup
        self.closeBtn.setShadow()
        
        //Image Setup
        self.displayImg.layer.cornerRadius = 30
        self.displayImg.clipsToBounds = true
        self.displayImg.setShadow()
        
        addSubview(self.view)
    }
    
    
    func setInfo(info: GalleryCard){
        
        //The image displayed
        self.displayImg.image = UIImage(named: info.imageName)
        
        //The name and Year
        self.yearLbl.text = "\(info.cardName) - \(info.yearToString())"
        self.yearLbl.font = self.yearLbl.font.fontWithSize(0.03*UIScreen.mainScreen().bounds.height)
        
        //The Description
        self.descriptionText.scrollEnabled = false
        self.descriptionText.text = info.description
        self.descriptionText.font = UIFont(name: "Familiar PRO", size: 0.020*UIScreen.mainScreen().bounds.height)
        self.descriptionText.textColor = UIColor.whiteColor()
        self.descriptionText.textAlignment = .Justified
        self.descriptionText.contentOffset = CGPointZero
        self.descriptionText.scrollRangeToVisible(NSRange(location:0, length:0))
        self.descriptionText.scrollEnabled = true
        self.descriptionText.userInteractionEnabled = true
    }
    
    func scroolDescriptionUp(){
        self.descriptionText.contentOffset = CGPointZero
        self.descriptionText.scrollRangeToVisible(NSRange(location:0, length:0))
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "GalleryPopUp", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
 
    @IBAction func closePopUp(sender: AnyObject) {
        self.closeBtn.releaseButton()
        
        if delegate != nil {
         self.delegate?.popClosed(sender)
        }
    }
    
    @IBAction func buttonPressed(sender: AnyObject) {
        self.closeBtn.pressButton()
    }
}
